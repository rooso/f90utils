!
! Extract interesting information from the logfile
! Author: J. Gabor
!
PROGRAM logfile_extract
  IMPLICIT NONE

  INTEGER, PARAMETER :: FILENAME_MAX = 1000, MAXLINE = 200, NSTORE = 10
  INTEGER :: narg, ierr, file_unit, line_num, i
  CHARACTER(LEN=FILENAME_MAX) :: infile, flag
  CHARACTER(LEN=MAXLINE) :: line, extra, extra2
  CHARACTER(LEN=MAXLINE), DIMENSION(NSTORE) :: oldlines
  LOGICAL :: keep, acc_keep, skipfirst
  real(kind=8) :: time, walltime, dt_walltime
  real :: dt
  INTEGER :: total_grids, ngrids_ilevel
  INTEGER :: coarse_step_counter

  ! This counter is used to track which BH each "gasmass" line (or "Angmom" line,
  ! or "partmass" line) corresponds to.  The gasmass lines in the log files do not
  ! specify which BH they belong to, but they are listed in the same order as the
  ! BHs.  So each time a new coarse timestep occurs, the BHs will be listed in order,
  ! so we can just re-set this counter to 1.  Then each subsequent gasmass line 
  ! during that timestep will be a subsequent BH.
  INTEGER :: bh_counter_for_gasmass

  coarse_step_counter = 0

  ! check number of arguments
  narg = iargc()
  IF (narg < 2) THEN 
     WRITE(*,*) "usage: logfile_extract logfilename flag"
     WRITE(*,*) "  flag -- string telling what info to extract"
     WRITE(*,*) "     e.g. 'accretion', 'timestep', 'BH', 'gasmass',"
     WRITE(*,*) "           'partmass', 'sfr', 'bondi_params'"
     STOP
  ENDIF

  ! read command-line arguments
  CALL getarg(1, infile)
  CALL getarg(2, flag)

  ! open input file
  file_unit = 60
  OPEN(file_unit, file = infile)

  line_num = 0
  walltime=0
  acc_keep = .false.

  ! read each line of the input file
  DO
     READ(file_unit, '(A)', iostat=ierr) line
     
     ! have we reached end-of-file?
     IF (ierr < 0) THEN 
        EXIT
     ENDIF

     ! increment line counter
     line_num = line_num+1
     keep = .false.
     skipfirst = .false.

!     IF (line_num == 100) write(*,*) "Getting there"
!     IF (line_num < 50) write(*,*) TRIM(line)

     ! find the simulation time
     ! check for the time at the beginning (after restart)
     IF( line(1:17) == " Restarting at t=" ) THEN
        read(line(18:36), *) time
     ENDIF

     ! check for time at fine timesteps
     IF( line(1:11) == " Fine step=") THEN
        ! read the time from "line" into the REAL "time" variable
        read(line( INDEX(line, " t=")+3 : INDEX(line, " t=")+3+12 ), *) time
     ENDIF

     ! check for the real, wallclock time since last step, in s
     IF(line(1:13) == " Time elapsed") THEN
        IF(line(21:25) == "start") THEN 
           read(line(31:45), *) dt_walltime
!           write(*,*) "start"
        ELSE
           IF(line(21:24) == "last") THEN
              read(line(38:55),*) dt_walltime
!              write(*,*) "last"
           ENDIF
        ENDIF
!        write(*,*) line(38:55), dt_walltime
        walltime = walltime + dt_walltime

        ! This line with the walltime signals the start of a new 
        ! coarse timestep, and thus another printout of the BH info
        coarse_step_counter = coarse_step_counter + 1
        bh_counter_for_gasmass = 1
     ENDIF

     ! check for the number of grids, adding them together
     IF(line(1:10) == " Level  1 ") THEN 
        total_grids = 0
!        WRITE(*,*) "Level 1"
     ENDIF
     IF(line(1:7) == " Level ") THEN
!        write(*,*) line
        read(line(15:25),*) ngrids_ilevel
        total_grids = total_grids + ngrids_ilevel
!        write(*,*) ngrids_ilevel
     ENDIF

!     if(line_num > 100) exit

     !----------------------------------------------------
     ! decide if we will keep this line or throw it away
     SELECT CASE(flag)
     CASE("timestep")
        IF( line(1:11) == " Fine step=") THEN
           ! extract the timestep from the line
!!           write(*,*) line( INDEX(line, " dt=")+3 : INDEX(line, " dt=")+3+11 )

           read(line( INDEX(line, " dt=")+4 : INDEX(line, " dt=")+4+11 ), *) dt
           WRITE(*,'(2ES14.7, I11, ES9.2)') walltime, time, total_grids, dt

        ENDIF
     CASE("accretion")
        ! identify the accretion lines by the labels coming before it
        IF( line(1:10) == " =========" ) THEN
           IF( oldlines(1)(1:18) == " Id     Mass(Msol)") THEN
              acc_keep = .true.
              skipfirst = .true.
           ELSE   !! this line must be the closing '======'
              acc_keep = .false.
           ENDIF  !! endelse
        ENDIF
        IF( acc_keep .and. (skipfirst .eqv. .false.)) THEN
              ! ok, let's keep this line for output
              keep = .true.

              ! put the simulation time into the "extra" string
              write( extra,*) time
              write( extra2,*) walltime
              
              ! combine the simulation time and walltime
              extra = TRIM(extra2) // TRIM(extra)
        ENDIF
        skipfirst = .false.
     CASE("BH")
        ! identify the BH lines by the label "BH"
        IF( line(1:5) == "BH  1") THEN
           ! ok, let's keep this line for output
           keep = .true.

           ! get rid of the "BH" label
           line = line(4:)

           ! put the simulation time into the "extra" string
           write( extra,*) time
           write( extra2,*) walltime
           
           ! combine the simulation time and walltime
           extra = TRIM(extra2) // TRIM(extra)           

        ENDIF
     CASE("gasmass")
        ! Identify gas mass lines
        IF( line(1:8) == "Gas mass") THEN 
           ! keep this line for output
           keep = .true.
           
           ! get rid of the "Gas mass" label
           line = line(9:)

           ! put the simulation time into the "extra" string
           write( extra, "(f12.8, I5)") time, bh_counter_for_gasmass
!!           write( extra2,*) walltime
           
           ! combine the simulation time and walltime
!!           extra = TRIM(extra2) // TRIM(extra)       

           bh_counter_for_gasmass = bh_counter_for_gasmass + 1
        ENDIF
     CASE("angmom")
        ! Identify angular momentum lines
        IF( line(1:7) == "Angmom ") THEN
           keep = .true.

           ! get rid of "Angmom" label, but keep the number which indicates direction
           line = line(7:)
           
!!!           write(*,*) "LINE", line(2:2)

           ! Put simulation time and BH ID into the "extra" string
           write( extra, "(f12.8, I5)") time, bh_counter_for_gasmass
           
           ! update the BH counter when finished
           IF( line(2:2) == '3') THEN
              bh_counter_for_gasmass = bh_counter_for_gasmass + 1
           ENDIF
        ENDIF
     CASE("partmass")
        ! Identify partmass mass lines
        IF( line(1:9) == "Part mass") THEN 
           ! keep this line for output
           keep = .true.
           
           ! get rid of the "Part mass" label
           line = line(10:)

           ! put the simulation time into the "extra" string
           write( extra,"(f12.8, I5)") time, bh_counter_for_gasmass
!!           write( extra2,*) walltime
           
           ! combine the simulation time and walltime
!!           extra = TRIM(extra2) // TRIM(extra)       
           bh_counter_for_gasmass = bh_counter_for_gasmass + 1
        ENDIF
     CASE("sfr")
        IF( line(14:22) == " New star") THEN 
           keep = .true.
           line = line(24:)
           write( extra,*) time
        ENDIF
     CASE("bondi_params")
        IF( line(1:12) == 'bondi params') THEN
           keep = .true.
           line = line(13:)
           write( extra,*) time
        END IF
     END SELECT

     ! write out the result to STDOUT
     ! This only applies to cases where we print out the whole line
     ! e.g. for "accretion"
     IF(keep) write(*,'(A)') TRIM(extra) // TRIM(line)

     ! Store this line as one of the previous ten, bumping the rest
     DO i=NSTORE, 2, -1
        oldlines(i) = oldlines(i-1)
     ENDDO
     oldlines(1) = line
     
  ENDDO

!  WRITE(*,*) "done", line_num
  CLOSE(file_unit)

END PROGRAM logfile_extract
