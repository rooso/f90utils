program sfr_amr_prog

  !ce programme parcourt les cellules du cube AMR et calcule le sfr initial

  implicit none

  real(kind=8)::pi=2.d0*acos(0.d0)
  real(kind=8)::var1, var2, var6, var7, var8, var9
  integer(kind=8)::number_of_leaves=0, i, j, j_min, ierr
  real(kind=8)::boxlen, l_max, number
  real(kind=8),dimension(:),allocatable::x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, &
       &temperature_amr, cell_size_amr
  integer(kind=8),dimension(:),allocatable::ilevel_amr
  character(len=300)::file_amr
  character(len=2)::lm
  character(len=6)::sim, fb_case, sm
  logical::only_once=.true.
  real(kind=8)::x_center,y_center,z_center
  real(kind=8)::time1,time2, time_before, time_loop


  !SFR et masse
  real(kind=8),dimension(:),allocatable::SFR100_amr, SFR10_amr, SFR1_amr, SFR0_amr
  real(kind=8)::M_tot_amr=0, M_sfr100_amr=0, M_sfr10_amr=0, M_sfr1_amr=0,  M_sfr0_amr=0, scale_mass
  real(kind=8)::epsilon=0.01d0,n_kennicutt=1.5d0,G_grav=6.67d-11,hcc_to_kgm3=1.67d-21,kgs_to_Msunyr=1.58d-23,kpc_to_m=1.d0/3.24d-20
  real(kind=8)::cst_SFR,scale_SFR, SFR_amr_max, scale_SFR_max
  real(kind=8)::count_sfr100_amr=0, count_sfr10_amr=0, count_sfr1_amr=0, count_sfr0_amr=0

  !gaz chaud
  real(kind=8)::M_gt5_4K=0, M_gt1_5K=0, M_gt1_6K=0, M_gt1_7K=0
  real(kind=8)::V_gt5_4K=0, V_gt1_5K=0, V_gt1_6K=0, V_gt1_7K=0, V_tot_amr=0

  integer(kind=8)::len_host,status_host
  character(len=15)::host
  logical::curie=.false.,mac=.false.,irfucoast=.false., curie2=.false.
  real(kind=8)::T_polytrope,alpha_jeans,density_0

  call GET_ENVIRONMENT_VARIABLE("HOSTNAME", host)
  print*, trim(host)

  if (trim(host) == 'curie51') then
     curie = .true.
  else if (trim(host) == 'sapmcr111' .or. trim(host) == '') then
     mac = .true.
  else if (trim(host) == 'irfucoast') then
     irfucoast = .true.
  else if (trim(host) == 'curie71') then
     curie2 = .true.
  else
     print*, 'Attention, machine non reconnue !'
     stop
  endif

  call CPU_TIME(time1)

  call getarg(1,sim)
  call getarg(2,lm)

  if (sim == '' .and. lm == '') then
     print*, "Il faut entrer le numero de l'output et lmax !"
     stop
  endif

if (lm == '') lm='13'

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if (trim(lm) == '12') then
host = 'cloudy_lores'
mac = .false.
endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  print*, 'Calcul du SFR initial dans la boite AMR'

  sm=sim

  if (sim == '00085' .or. sim == '00075n' &
       &.or. sim == '00090' .or. sim == '00120' .or. sim == '00130' .or. sim == '00148') then
     fb_case = 'No'
     if (sim == '00075n') then
        sm = '00075'
     endif
     if (mac) then
        open(15,file='/Users/oroos/Post-stage/LOPs'//trim(sim)//'/initial_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
        open(16,file='/Users/oroos/Post-stage/LOPs'//trim(sim)//'/initial_individual_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')

     else if (curie) then
        open(15,file='/ccc/work/cont005/gen2192/juneaus/initial_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
        open(16,file='/ccc/work/cont005/gen2192/juneaus/initial_individual_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')

     else if (curie2) then
        open(15,file='/ccc/work/cont003/dsm/rooso/initial_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
        open(16,file='/ccc/work/cont003/dsm/rooso/initial_individual_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')

     else if (irfucoast) then
        open(15,file='/gpfs/data2/oroos/initial_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
        open(16,file='/gpfs/data2/oroos/initial_individual_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
     endif

  endif

  if (sim == '00100' .or. sim == '00075w' &
       &.or. sim == '00108' .or. sim == '00150' .or. sim == '00170' .or. sim == '00210' .or. lm =='12') then
     fb_case = 'With'
     if (sim == '00075w') then
        sm = '00075'
     endif
     print*, 'Attention, cet output vient de la simu AVEC FEEDBACK, est-ce voulu ??'

     if (mac) then
        open(15,file='/Users/oroos/Post-stage/LOPs'//trim(sim)//'/thermal_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
     else if (curie) then
        open(15,file='/ccc/work/cont005/gen2192/juneaus/thermal_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
     else if (curie2) then
        open(15,file='/ccc/work/cont003/dsm/rooso/thermal_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')
     else if (irfucoast) then
        open(15,file='/gpfs/data2/oroos/thermal_mass_sfr_'&
             & //trim(sim)//'_all.dat',form='formatted')

else if (host == 'cloudy_lores') then
open(15,file='/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/LOPs'//trim(sim)//'/thermal_mass_sfr_'&
& //trim(sim)//'_all.dat',form='formatted')
     endif

  endif


  if (mac) then
     open(9, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (curie) then
     open(9, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (curie2) then
     open(9, file="/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (irfucoast) then
     open(9, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
else if (host == 'cloudy_lores') then
open(9,file='/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/output_'//trim(sm)//&
          &"/sink_"//trim(sm)//".out",form='formatted')

  endif

  do j = 1,4
     read(9,*)
  enddo
  read(9,*) var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  write(*,*) "Position of the AGN : ",  x_center, y_center, z_center
  close(9)

  if (mac) then
     open(10, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (curie) then
     open(10, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (curie2) then
     open(10, file="/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (irfucoast) then
     open(10, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
else if (host == 'cloudy_lores') then
open(10,file='/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/statistics_' &
          &//trim(sim)//".ascii.lmax"//trim(lm),form='formatted')
  endif

  read(10,*) number_of_leaves, boxlen, l_max
  write(*,*) "n_leaves, boxlen (kpc), lmax ", number_of_leaves, boxlen, l_max
  !number     !kpc     !number

  !write(*,*) fb_case, sim, lm
  !write(*,*) "/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
  !&"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)

  !fichier contenant la liste des cellules de la boite AMR
  if (mac) then
     file_amr = "/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (curie) then
     file_amr = "/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (curie2) then
     file_amr = "/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (irfucoast) then
     file_amr = "/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
else if (host == 'cloudy_lores') then
     file_amr = '/Users/oroos/Post-stage/SIMUS/Simu_Cloudy_lores/output/gas_part_' &
          &//trim(sim)//".ascii.lmax"//trim(lm)
  endif


  open(11, file=trim(file_amr), form="formatted",iostat=ierr,status="old")

  if (ierr/=0) then
     write(*,*) "Impossible d'ouvrir le fichier "//trim(file_amr)
     stop
  endif

  allocate(x_amr(1:number_of_leaves))
  allocate(y_amr(1:number_of_leaves))
  allocate(z_amr(1:number_of_leaves))
  allocate(vx_amr(1:number_of_leaves))
  allocate(vy_amr(1:number_of_leaves))
  allocate(vz_amr(1:number_of_leaves))
  allocate(density_amr(1:number_of_leaves))
  allocate(temperature_amr(1:number_of_leaves))
  allocate(cell_size_amr(1:number_of_leaves))
  allocate(ilevel_amr(1:number_of_leaves))
  allocate(SFR100_amr(1:number_of_leaves))
  allocate(SFR10_amr(1:number_of_leaves))
  allocate(SFR1_amr(1:number_of_leaves))
  allocate(SFR0_amr(1:number_of_leaves))

  scale_mass = hcc_to_kgm3*kpc_to_m**3/2.0d30 !n(H/cc) and V(kpc^3) -> M(M_sun)
  scale_SFR_max = 1.67d-27/2.0d30*(1.d0/3.24d-22)**3*1d-9 ! "           "       -> SFR(M_sun/yr)

  !constantes pour le SFR :
  cst_SFR = sqrt(32.d0*G_grav/(3.d0*pi))
  scale_SFR = cst_SFR*hcc_to_kgm3**n_kennicutt*kpc_to_m**3*kgs_to_Msunyr

  !print*, epsilon*cst_SFR*hcc_to_kgm3**n_kennicutt*kpc_to_m**3*kgs_to_Msunyr


  call CPU_TIME(time_before)


  !correction du polytrope de jeans (deja faite pour les donnees cloudy)
  alpha_jeans = (0.041666d0*sqrt(32*pi)*(boxlen*1d3/2**l_max)**2) !boxlen en pc
  density_0 = 900.d0/alpha_jeans !density(T = 900 K) H/cc
  !print*, 'density_0, alpha_jeans : ', density_0, alpha_jeans



  do i = 1, number_of_leaves

     read(11,'(9d20.12,1I5)') x_amr(i), y_amr(i), z_amr(i), vx_amr(i), vy_amr(i), vz_amr(i), &
          &density_amr(i), temperature_amr(i), cell_size_amr(i), ilevel_amr(i)
     !write(*,*) x(1), y(1), z(1), vx(1), vy(1), vz(1), rho(1), temp(1), cell_size(1), ilevel(1)
     !kpc      !km/s                                                   !H/cc  !K       !kpc   !number
     !x, y, z, vx(mass-weighted), vy(mass-weighted), vz(mass-weighted), rho, temp, cell_size, level

     if (density_amr(i) == 0 .or. temperature_amr(i)  == 0 .or. cell_size_amr(i) == 0 .or.  ilevel_amr(i) == 0) then
        print*, 'AMR : Attention, valeurs lues nulles. ', i
        print*, density_amr(i), temperature_amr(i), cell_size_amr(i), ilevel_amr(i)
        stop
     endif

     call CPU_TIME(time_loop)
     if(time_loop - time_before > 25.d0) then
        write(*,*) real(i)/real(number_of_leaves)*100, '% proceeded...'
        time_before = time_loop
     endif

     !correction du polytrope de jeans !nb : deja fait pour les donnees des lop
     if (density_amr(i) >= density_0) then
        T_polytrope = alpha_jeans*density_amr(i)
        if (temperature_amr(i) <= 2.d0*T_polytrope) then
           temperature_amr(i) = 900 !K
        endif
     endif


     !print*, x_amr(i), y_amr(i), z_amr(i), cell_size_amr(i), i


     !calcul du SFR correspondant :
     SFR100_amr(i) = 0
     SFR10_amr(i) = 0
     SFR1_amr(i) = 0
     SFR0_amr(i) = 0



     !if (density_amr(i) >= 10 .and. temperature_amr(i) <= 1d4) then
     !    SFR_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
     !    SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
     !
     !    !print*, SFR_amr(i), SFR_amr_max
     !    if (SFR_amr(i) > SFR_amr_max) then
     !        SFR_amr(i) = SFR_amr_max
     !        !print*, 'Reduction'
     !    endif
     !
     !!SFR_max (M_sun/yr) = epsilon_max*9.9d0*density_amr(10^9 M_sun/kpc^3)*V_cell(kpc^3) (from Jared, used to compute SFR_max in Ramses)
     !!avec 10^9M_sunkpc3_to_Hcc = 10^9*2d30/1.67d-27*(3.24d-22)^3 = 407.33d0 (et on utilise l'inverse)
     !count_sfr_amr = count_sfr_amr + 1
     !M_sfr_amr = M_sfr_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun
     !endif

     if (temperature_amr(i) <= 1d4) then

        SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
        SFR0_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
        !print*, SFR0_amr(i), SFR_amr_max
        if (SFR0_amr(i) > SFR_amr_max) then
           SFR0_amr(i) = SFR_amr_max
           !print*, 'Reduction'
        endif
        count_sfr0_amr = count_sfr0_amr + 1
        M_sfr0_amr = M_sfr0_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun

        if (density_amr(i) >= 1) then
           SFR1_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
           !print*, SFR1_amr(i), SFR_amr_max
           if (SFR1_amr(i) > SFR_amr_max) then
              SFR1_amr(i) = SFR_amr_max
              !print*, 'Reduction'
           endif
           count_sfr1_amr = count_sfr1_amr + 1
           M_sfr1_amr = M_sfr1_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun

           if (density_amr(i) >= 10) then
              SFR10_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
              !print*, SFR10_amr(i), SFR_amr_max
              if (SFR10_amr(i) > SFR_amr_max) then
                 SFR10_amr(i) = SFR_amr_max
                 !print*, 'Reduction'
              endif
              count_sfr10_amr = count_sfr10_amr + 1
              M_sfr10_amr = M_sfr10_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun

              if (density_amr(i) >= 100) then
                 SFR100_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
                 !print*, SFR100_amr(i), SFR_amr_max
                 if (SFR100_amr(i) > SFR_amr_max) then
                    SFR100_amr(i) = SFR_amr_max
                    !print*, 'Reduction'
                 endif
                 count_sfr100_amr = count_sfr100_amr + 1
                 M_sfr100_amr = M_sfr100_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun

              endif
           endif
        endif
     endif


     if (sim == '00085' .or. sim == '00075n' &
          &.or. sim == '00090' .or. sim == '00120' .or. sim == '00130' .or. sim == '00148') then
        write(16,*) SFR100_amr(i), SFR10_amr(i), SFR1_amr(i), SFR0_amr(i), cell_size_amr(i), density_amr(i)
     endif


     !calcul de la masse totale : M = rho*cell_size**3 /!\ rho en H/cc et cell_size en kpc...
     ! 1 cm = 3.24d-22 kpc
     ! M_H = 1.67d-27 kg
     ! M_sun = 2.0d30 kg

     M_tot_amr = M_tot_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass !M_sun
     V_tot_amr = V_tot_amr + cell_size_amr(i)**3                           !kpc

     if (temperature_amr(i) > 5d4) then
        M_gt5_4K = M_gt5_4K + density_amr(i)*cell_size_amr(i)**3*scale_mass
        V_gt5_4K = V_gt5_4K + cell_size_amr(i)**3

        if (temperature_amr(i) > 1d5) then
           M_gt1_5K = M_gt1_5K + density_amr(i)*cell_size_amr(i)**3*scale_mass
           V_gt1_5K = V_gt1_5K + cell_size_amr(i)**3

           if (temperature_amr(i) > 1d6) then
              M_gt1_6K = M_gt1_6K + density_amr(i)*cell_size_amr(i)**3*scale_mass
              V_gt1_6K = V_gt1_6K + cell_size_amr(i)**3

              if (temperature_amr(i) > 1d7) then
                 M_gt1_7K = M_gt1_7K + density_amr(i)*cell_size_amr(i)**3*scale_mass
                 V_gt1_7K = V_gt1_7K + cell_size_amr(i)**3
              endif
           endif
        endif
     endif

     !print*, x_amr(i), y_amr(i), z_amr(i)

  end do


  print*, 'M_tot=',M_tot_amr,' M_sun, V_tot=', V_tot_amr, ' kpc^3'
  print*, 'Mass fraction :'
  print*, '  T > 5d4 K', '                T > 1d5 K', '                  T > 1d6 K', '                 T > 1d7 K'
  print*, M_gt5_4K/M_tot_amr, M_gt1_5K/M_tot_amr, M_gt1_6K/M_tot_amr, M_gt1_7K/M_tot_amr
  print*, 'Volume fraction :'
  print*, ' T > 5d4 K', '                 T > 1d5 K', '                 T > 1d6 K', '                 T > 1d7 K'
  print*, V_gt5_4K/V_tot_amr, V_gt1_5K/V_tot_amr, V_gt1_6K/V_tot_amr, V_gt1_7K/V_tot_amr

  print*, 'Moyenne de la densite sur la boite :', sum(density_amr)/max(1,size(density_amr)), ' H/cc.'


  print*, 'Nb de cellules avec SFR100>0 :  '
  print*, 'AMR : ', count_sfr100_amr
  print*, 'Nb de cellules avec SFR10>0 :  '
  print*, 'AMR : ', count_sfr10_amr
  print*, 'Nb de cellules avec SFR1>0 :  '
  print*, 'AMR : ', count_sfr1_amr
  print*, 'Nb de cellules avec SFR0>0 :  '
  print*, 'AMR : ', count_sfr0_amr

  write(15,*) '-------------------------------------------------------------------------------------'
  write(15,*) 'Pour les cellules AMR : on a '
  write(15,*)  'Masse totale contenue initialement dans les cellules AMR : ', M_tot_amr, ' M_sun.'
  write(15,*)  'SFR100 total initial des cellules AMR : ', sum(SFR100_amr), 'M_sun/yr.'
  write(15,*)  'Nombre de cellules avec SFR100 > 0 : ', count_sfr100_amr, ' sur ', number_of_leaves
  write(15,*)  'soit ', count_sfr100_amr/number_of_leaves*100.d0, ' % en nombre de cellules'
  write(15,*)  'et ', M_sfr100_amr/M_tot_amr*100.d0, '% en masse.'
  write(15,*)  'SFR10 total initial des cellules AMR : ', sum(SFR10_amr), 'M_sun/yr.'
  write(15,*)  'Nombre de cellules avec SFR10 > 0 : ', count_sfr10_amr, ' sur ', number_of_leaves
  write(15,*)  'soit ', count_sfr10_amr/number_of_leaves*100.d0, ' % en nombre de cellules'
  write(15,*)  'et ', M_sfr10_amr/M_tot_amr*100.d0, '% en masse.'
  write(15,*)  'SFR1 total initial des cellules AMR : ', sum(SFR1_amr), 'M_sun/yr.'
  write(15,*)  'Nombre de cellules avec SFR1 > 0 : ', count_sfr1_amr, ' sur ', number_of_leaves
  write(15,*)  'soit ', count_sfr1_amr/number_of_leaves*100.d0, ' % en nombre de cellules'
  write(15,*)  'et ', M_sfr1_amr/M_tot_amr*100.d0, '% en masse.'
  write(15,*)  'SFR0 total initial des cellules AMR : ', sum(SFR0_amr), 'M_sun/yr.'
  write(15,*)  'Nombre de cellules avec SFR0 > 0 : ', count_sfr0_amr, ' sur ', number_of_leaves
  write(15,*)  'soit ', count_sfr0_amr/number_of_leaves*100.d0, ' % en nombre de cellules'
  write(15,*)  'et ', M_sfr0_amr/M_tot_amr*100.d0, '% en masse.'
  !- jusqu'à présent, mes cartes et valeurs de SFR montraient les cas 'pas de FB dans la simu + FB cloudy' versus 
  !'FB dans la simu + FB cloudy' et donc T_f = max (T_cloudy, T_amr), T_i = T_amr (la meme)

  !- maintenant on veut l'impact du FB global simu+cloudy
  !donc on voudrait T_f = max(T_cloudy, T_amr_avec_fb) avec le calcul cloudy fait sur le cube avec fb, T_i = T_amr_sans_fb


  close(10)
  close(11)
  close(12)
  close(14)
  close(15)
  if (sim == '00085' .or. sim == '00075n' &
       &.or. sim == '00090' .or. sim == '00120' .or. sim == '00130' .or. sim == '00148') then

     close(16)
     print*, 'Fichier cree : initial_mass_sfr_'&
          & //trim(sim)//'_all.dat'
     print*, 'Fichier cree : initial_individual_sfr_'&
          & //trim(sim)//'_all.dat'
  else
     print*, 'Fichier cree : initial/thermal_mass_sfr_'&
          & //trim(sim)//'_all.dat'
  endif

  deallocate(x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, &
       & cell_size_amr, ilevel_amr, SFR100_amr, SFR10_amr, SFR1_amr, SFR0_amr)


  print*, 'Fin du programme, tout est OK.'

end program sfr_amr_prog
