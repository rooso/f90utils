program line_of_prop_cone

  !computes lines of propagation at l_max for snapshot XXXXX in half of the box (up/down)

  implicit none

!!!!test d'optimisation
  real(kind=8)::radius_amr_1, radius_amr_2, theta_amr, phi_amr, alpha_solid
  real(kind=8)::theta_amr_1, phi_amr_1, theta_amr_2, phi_amr_2
  real(kind=8)::x_i, y_i, z_i, test_min

  real(kind=8)::time1,time2, time_before, time_loop

  integer(kind=8)::i, t, count_lop = 0, t_max
  integer,dimension(:,:),allocatable::bool_t
  logical:: in_condition
  logical,dimension(:),allocatable::in_cell_i

  real(kind=8)::var1, var2, var6, var7, var8, var9

  character(len=2)::half, lm
  character(len=6)::sim, fb_case, sm


  real(kind=8)::pi

  !statistics
  integer(kind=8)::number_of_leaves
  real(kind=8)::boxlen, l_max

  !data
  real(kind=8),dimension(:),allocatable::x, y, z, vx, vy, vz, rho, temp, cell_size
  integer(kind=8),dimension(:),allocatable::ilevel, count_points
  real(kind=8)::cell_size_min
  real(kind=8)::x_tmp = 0, y_tmp = 0, z_tmp = 0, theta_tmp, phi_tmp
  integer(kind=8)::n = 0

  real(kind=8)::M_tot=0.d0

  !line of sight
  real(kind=8)::x_center,y_center,z_center
  real(kind=8),dimension(:),allocatable::x_border, y_border, z_border, theta_LOP, phi_LOP
  real(kind=8)::x_lop,y_lop,z_lop, depth, L
  real(kind=8),dimension(:),allocatable::dx, dy, dz, u, v, w

  integer(kind=8)::n_lop, LOP_number = 0, k, k_write, n_LOP_10percent
  character(len=10)::char_k, parm, time
  character(len=8)::date
  real(kind=8)::rand1, rand2, rand3
  real(kind=8)::theta, phi
  real(kind=8)::radius, theta_max, theta_min
  logical:: up=.false., dn=.false., cu=.false., cd=.false., dk=.false.
  logical:: is_random = .true., xbool, ybool, zbool, rot=.false., ten_percent=.false.

  !inclination of the cone
  real(kind=8)::alpha, beta, gamma_, x_rotate, y_rotate, z_rotate

!!!!!!!!!parametres en ligne de commande : nombre, haut/bas, snapshot/simulation, lmax

  integer(kind=8)::len_host,status_host
  character(len=15)::host
  logical::curie=.false.,mac=.false.,irfucoast=.false.

  call GET_ENVIRONMENT_VARIABLE("HOSTNAME", host)
  print*, trim(host)

  if (trim(host) == 'curie51') then
     curie = .true.
  else if (trim(host) == 'sapmcr134' .or. trim(host) == '') then
     mac = .true.
  else if (trim(host) == 'irfucoast') then
     irfucoast = .true.
     !else
     !print*, 'Attention, machine non reconnue !'
     !stop
  endif

  call CPU_TIME(time1)

  call getarg(2,half)
  call getarg(3,sim)
  call getarg(4,lm)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (trim(lm) == '12') then
     host = 'cloudy_lores'
  endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !moitie de la boite voulue : haut ou bas, ou tranche mince autour du disque
  if (half == 'up') then
     up = .true.
  else if (half == 'dn') then
     dn = .true.
  else if (half == 'dk') then
     dk=.true.
  else if (half == 'cu') then
     cu=.true.
  else if (half == 'cd') then
     cd=.true.
  else
     print*, "Attention, il faut entrer le nombre de LOP, puis up, dn (down) ou  dk (disk), ou cu/cd pour restreindre le cone !"
     stop
  endif

  sm=sim

  if (sim == '00085' .or. sim == '00075n' &
       &.or. sim == '00090' .or. sim == '00120' .or. sim == '00130' .or. sim == '00048') then
     fb_case = 'No'
     if (sim == '00075n') then
        sm = '00075'
     endif
  endif

  if (sim == '00100' .or. sim == '00075w' &
       &.or. sim == '00108' .or. sim == '00150' .or. sim == '00170' .or. sim == '00210') then
     fb_case = 'With'
     if (sim == '00075w') then
        sm = '00075'
     endif
  endif

  !print*, sim
  !print*, "/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
  !       &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm)

  if (mac) then
     open(10, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(11, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(12, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")

     !ouverture des fichiers de resultats
     open(13, file="/Users/oroos/Post-stage/LOPs"//trim(sim)//&
          &"/LOP_plane_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")
     open(14, file="/Users/oroos/Post-stage/LOPs"//trim(sim)//&
          &"/number_of_LOPs_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")

  else if(curie) then
     !ouverture des fichiers de donnees
     open(10, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(11, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(12, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")

     !ouverture des fichiers de resultats
     open(13, file="/ccc/work/cont005/gen2192/juneaus/3D_LOPs/LOPs"//trim(sim)//&
          &"/LOP_plane_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")
     open(14, file="/ccc/work/cont005/gen2192/juneaus/3D_LOPs/LOPs"//trim(sim)//&
          &"/number_of_LOPs_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")

  else if (irfucoast) then
     !ouverture des fichiers de donnees
     open(10, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(11, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
     open(12, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")

     !ouverture des fichiers de resultats
     open(13, file="/gpfs/data2/oroos/3D_LOPs/LOPs"//trim(sim)//&
          &"/LOP_plane_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")
     open(14, file="/gpfs/data2/oroos/3D_LOPs/LOPs"//trim(sim)//&
          &"/number_of_LOPs_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")

  else if (host == 'cloudy_lores') then
     open(10, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/statistics_"&
          &//trim(sim)//".ascii.lmax"//trim(lm), form="formatted")
     open(11, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/gas_part_"&
          &//trim(sim)//".ascii.lmax"//trim(lm), form="formatted")
     open(12, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/output_"&
          &//trim(sm)//"/sink_"//trim(sm)//".out", form="formatted")

     !ouverture des fichiers de resultats
     open(13, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/LOPs"//trim(sim)//&
          &"/LOP_plane_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")
     open(14, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/LOPs"//trim(sim)//&
          &"/number_of_LOPs_"//trim(sim)//trim(half)//".ascii", &
          & form="formatted")


  endif



  pi =  2.d0*acos(0.d0) !3.141592654d0
  !theta_max = pi/2.d0
  !print*, theta_max


  !nombre de LOP voulu :
  call getarg(1,parm)
  if (parm == '') then
     print*, "Attention, il faut entrer le nombre de LOP souhaitees !"
     stop
  endif
  read(parm,*) n_LOP
  !n_LOP = 50
  write(14,*) n_LOP

  allocate(count_points(1:n_LOP))

  do k = 1, n_LOP
     write(char_k,'(I5)') k
     char_k = adjustl(char_k)
     k_write = k + 30

     call date_and_time(DATE=date,TIME=time)

     if (mac) then
        open(k_write, &
             & file="/Users/oroos/Post-stage/LOPs"//trim(sim)//"/density_profile_"//trim(sim)//"_LOP"&
             &//trim(char_k)//trim(half)//"__d"//trim(date)//"t"//trim(time)//".ascii", &
             & form="formatted")
     else if (curie) then
        open(k_write, &
             & file="/ccc/work/cont005/gen2192/juneaus/3D_LOPs/LOPs"//trim(sim)//"/density_profile_"//trim(sim)//"_LOP"&
             &//trim(char_k)//trim(half)//"__d"//trim(date)//"t"//trim(time)//".ascii", &
             & form="formatted")
     else if (irfucoast) then
        open(k_write, &
             & file="/gpfs/data2/oroos/3D_LOPs/LOPs"//trim(sim)//"/density_profile_"//trim(sim)//"_LOP"&
             &//trim(char_k)//trim(half)//"__d"//trim(date)//'t'//trim(time)//".ascii", &
             & form="formatted")
     else if (host == 'cloudy_lores') then
        open(k_write, &
             & file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/LOPs"&
             &//trim(sim)//"/lores_density_profile_"//trim(sim)//"_LOP"&
             &//trim(char_k)//trim(half)//"__d"//trim(date)//'t'//trim(time)//".ascii", &
             & form="formatted")
     endif


     count_points(k) = 0
  enddo

  !lecture des statistiques
  read(10,*) number_of_leaves, boxlen, l_max
  write(*,*) "n_leaves, boxlen (kpc), lmax ", number_of_leaves, boxlen, l_max
  !number           !kpc     !number
  close(10)

  allocate(x(1:number_of_leaves+1))   !ce tableau est cree volontairement avec un point en trop :
  allocate(y(1:number_of_leaves+1))   !afin de prendre en compte d'eventuels dernier et avant-dernier points identiques,
  allocate(z(1:number_of_leaves+1))   !il faut pouvoir aller un pas plus loin afin de moyenner
  allocate(vx(1:number_of_leaves+1))
  allocate(vy(1:number_of_leaves+1))
  allocate(vz(1:number_of_leaves+1))
  allocate(rho(1:number_of_leaves+1))
  allocate(temp(1:number_of_leaves+1))
  allocate(cell_size(1:number_of_leaves+1))
  allocate(ilevel(1:number_of_leaves+1))

  x(number_of_leaves+1) = 0.d0  !initialisation du point en trop "point fantome"
  y(number_of_leaves+1) = 0.d0
  z(number_of_leaves+1) = 0.d0
  vx(number_of_leaves+1) = 0.d0
  vy(number_of_leaves+1) = 0.d0
  vz(number_of_leaves+1) = 0.d0
  rho(number_of_leaves+1) = 0.d0
  temp(number_of_leaves+1) = 0.d0
  cell_size(number_of_leaves+1) = 0.d0
  ilevel(number_of_leaves+1) = 0.d0


  !construction de la ligne de visee
  !x_center = boxlen/2.d0; y_center = boxlen/2.d0; z_center = boxlen/2.d0 !kpc
  do k = 1,4
     read(12,*)
  enddo
  read(12,*) var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  write(*,*) "Postition of the AGN : ",  x_center, y_center, z_center


  !exemple
  !a faire varier plus tard
  !coin superieur le plus eloigne
  !x_border = boxlen; y_border = boxlen; z_border = boxlen !kpc
  !milieu de la face verticale arriere (rq : la galaxie est dans le plan xy !)
  !x_border = boxlen/2.d0+.01d0; y_border = boxlen; z_border = boxlen/2.d0+.01d0 !kpc
  !write(*,*) "border ", x_border, y_border, z_border

  allocate(x_border(1:n_LOP))
  allocate(y_border(1:n_LOP))
  allocate(z_border(1:n_LOP))
  allocate(dx(1:n_LOP))
  allocate(dy(1:n_LOP))
  allocate(dz(1:n_LOP))
  allocate(in_cell_i(1:n_LOP))
  allocate(theta_LOP(1:n_LOP))
  allocate(phi_LOP(1:n_LOP))
  allocate(u(1:n_LOP))
  allocate(v(1:n_LOP))
  allocate(w(1:n_LOP))

  cell_size_min = boxlen/2.0d0**l_max !kpc

  !toutes les LOP auront pour extremite un point de la sphere centree sur l'AGN et inscrite dans le cube de la simulation
  radius = min(x_center - 0.d0, y_center - 0.d0, z_center - 0.d0, boxlen - x_center, boxlen - y_center, boxlen - z_center)
  !print*, "rayon de la sphere inscrite (kpc) : ", radius

  !angles definissant la rotation du cone autour de l'axe de la galaxie
  alpha = 0.d0 !angle de rotation autour de Oz
  beta = 0.d0  !angle de rotation autour de Oy
  gamma_ = 0.d0 !angle de rotation autour de Ox

  t_max = int(radius/cell_size_min)
  allocate(bool_t(0:t_max+1,1:n_LOP))

  !is_random = .false. !si is_random est faux, les LOP tirees sont toujours les memes

  if (is_random) call random_seed()

  do k = 1, n_LOP
     !tirage aleatoire de la ligne de visee a l'interieur d'un cone de demi-ouverture theta_max
     !on tire l'angle phi de facon uniforme entre 0 et 2pi

     !if (is_random) call random_seed()
     call random_number(rand1)
     phi = rand1*2.0d0*pi   !radians

     !on tire une variable uniformement entre 0 et 1 et on la transforme en theta tire proportionnellement a sin(theta) entre theta_min et theta_max
     !if (is_random) call random_seed()
     call random_number(rand2)
     if (k <= n_LOP_10percent .and. ten_percent) then
        theta_min = 0.d0
        theta_max = 10.d0*pi/180.d0!forcer 10% des LOP dans les 10 degres perpendiculaires au disque
     else if (dk) then
        theta_min = acos(50.d-3/radius) !disque = tranche de 50 pc de chaque cote du plan du BH
        theta_max = pi/2.d0
     else if (cu .or. cd) then
        theta_min = 0.d0
        theta_max = pi/6.d0      !lignes restreintes dans un cone de 30 degres de demi-ouverture
     else
        theta_min = 0.d0
        theta_max = pi/2.d0      !theta est tire aleatoirement entre 0 et pi/2 avec plus forte probabilite pour pi/2.d0 1 fois sur 2 on prend pi-theta
     endif                      !pour avoir le cone inferieur

     theta = acos(cos(theta_min) - (cos(theta_min) - cos(theta_max))*rand2) !radians
     !theta = pi/2.d0    !print*, "phi1 et 2, theta ", phi1, phi2, theta

     call random_number(rand3)

     if (dn .or. cd) then
        !si demande explicitement, ou dans 50% des cas, 
        !on prend le symetrique de la LOP dans le cone inferieur (plan de sym : la galaxie) : theta -> pi - theta
        theta_LOP(k) = pi-theta
        print*, k, " cone inferieur "
     else if (up .or. cu) then
        theta_LOP(k) = theta
        print*, k, " cone superieur "
     else if (dk .and. rand3 >= 0.5d0) then
        theta_LOP(k) = pi-theta
        print*, k, " cone inferieur "
     else if (dk .and. rand3 < 0.5d0) then 
        theta_LOP(k) = theta
        print*, k, " cone superieur "
     endif



     phi_LOP(k) = phi

     !print*, theta, theta_max, rand2

!!!!!!!!!!!!!! vecteurs directeurs
     u(k) = sin(theta_LOP(k))*cos(phi_LOP(k)) !== (x - x_c)/radius
     v(k) = sin(theta_LOP(k))*sin(phi_LOP(k))
     w(k) = cos(theta_LOP(k))

     x_border(k) = radius*u(k) + x_center
     y_border(k) = radius*v(k) + y_center
     z_border(k) = radius*w(k) + z_center

     print*, x_border(k), y_border(k), z_border(k)

     write(k + 30,*) 'theta, phi : ', theta_LOP(k), phi_LOP(k)
     write(k + 30,*) '---------------------------'

     if (rot) then
        !rotation pour prendre en compte l'inclinaison du cone par rapport au plan de la galaxie
        !R = R_x(gamma)*R_y(beta)*R_z(alpha)
        !x_rotate = (cos(alpha)*cos(beta)  +                0.d0              )*x_border(k) + (- sin(alpha)*cos(beta)  +                 0.d0             )*y_border(k) + (  sin(beta)           )*z_border(k)
        !y_rotate = (sin(alpha)*cos(gamma) + cos(alpha)*sin(beta)*sin(gamma))*x_border(k) + (  cos(alpha)*cos(gamma) - sin(alpha)*sin(beta)*sin(gamma))*y_border(k) + (- cos(beta)*sin(gamma))*z_border(k)
        !z_rotate = (sin(alpha)*sin(gamma) + cos(alpha)*sin(beta)*cos(gamma))*x_border(k) + (  cos(alpha)*sin(gamma) + sin(alpha)*sin(beta)*cos(gamma))*y_border(k) + (  cos(beta)*cos(gamma))*z_border(k)
        x_rotate = (cos(alpha)*cos(beta))*x_border(k) + (- sin(alpha)*cos(beta))*y_border(k) + (sin(beta))*z_border(k)

        y_rotate = (sin(alpha)*cos(gamma_) + cos(alpha)*sin(beta)*sin(gamma_))*x_border(k) &
             &    + (cos(alpha)*cos(gamma_) - sin(alpha)*sin(beta)*sin(gamma_))*y_border(k) &
             &    + (- cos(beta)*sin(gamma_))*z_border(k)

        z_rotate = (sin(alpha)*sin(gamma_) + cos(alpha)*sin(beta)*cos(gamma_))*x_border(k) &
             &    + (cos(alpha)*sin(gamma_) + sin(alpha)*sin(beta)*cos(gamma_))*y_border(k) &
             &    + (cos(beta)*cos(gamma_))*z_border(k)

        x_border(k) = x_rotate
        y_border(k) = y_rotate
        z_border(k) = z_rotate
     endif

     !LOP dans le plan galactique
     !x_border(k) = boxlen/2.d0+.01d0; y_border(k) = boxlen; z_border(k) = boxlen/2.d0+.01d0 !kpc

     !print*, "x_border, y_border, z_border ", x_border(k), y_border(k), z_border(k), phi1, phi2, theta
     write(13,*) x_border(k), y_border(k), z_border(k), phi, theta

     !longueur de la ligne de visee              si l'AGN est pile au centre de la boite, L = boxlen/2.d0

     t_max = int(radius/cell_size_min)
     !write(*,*) "eps_min, t_max, L ", cell_size_min, t_max, radius

     dx(k) = cell_size_min*(x_border(k) - x_center)/radius
     dy(k) = cell_size_min*(y_border(k) - y_center)/radius
     dz(k) = cell_size_min*(z_border(k) - z_center)/radius

  enddo
  !test
  !x_border(2) = x_border(1)
  !y_border(2) = y_border(1)
  !dx(2) = dx(1)
  !dy(2) = dy(1)
  !dz(2) = dz(1)

  !parcourir une fois toutes les positions de la liste et verifier si elles sont sur la l-o-s definie ci-dessus

  !boucle de lecture des donnees

  test_min = boxlen

  call CPU_TIME(time_before)

  do i = 1, number_of_leaves+1 !loop over number_of_leaves cells
     !print*, "Cell number ", i
     if (i /= number_of_leaves+1 ) then !ignorer le point fantome
        read(11,'(9d20.12,1I5)') x(i), y(i), z(i), vx(i), vy(i), vz(i), rho(i), temp(i), cell_size(i), ilevel(i)
        !write(*,*) x(1), y(1), z(1), vx(1), vy(1), vz(1), rho(1), temp(1), cell_size(1), ilevel(1)
        !kpc              !km/s                !H/cc   !K       !kpc          !number
        !x, y, z, vx(mass-weighted), vy(mass-weighted), vz(mass-weighted), rho, temp, cell_size, level
        !print*, cell_size(i), test_min
        if (cell_size(i) < test_min) then
           test_min = cell_size(i)
           !print*, 'moins !'
        endif
        if (cell_size(i) == 0.d0) print*, 'ouh ca c est pas bon !'
     endif

     x_i = x(i) - x_center
     y_i = y(i) - y_center
     z_i = z(i) - z_center


!!!!test d'optimisation : calculer coord de la cellule dans la boite pour reperer les lop qui passent dedans
     radius_amr_2 = sqrt(x_i**2 + y_i**2)
     radius_amr_1 = sqrt(radius_amr_2**2 + z_i**2)


     theta_amr = acos(z_i/radius_amr_1)
     if (y_i >= 0.d0) phi_amr = acos(x_i/radius_amr_2)
     if (y_i <  0.d0) phi_amr = 2.d0*pi - acos(x_i/radius_amr_2)
     !phi_amr = atan2(y_i,x_i)
     !if (phi_amr < 0) phi_amr = phi_amr + 2.d0*pi   !les deux blocs sont equivalents (ils donnent le meme resultat pour phi)


     !TEST
     if (x_i - radius_amr_1*cos(phi_amr)*sin(theta_amr) > 1d-5) print*, 'x est faux'
     if (y_i - radius_amr_1*sin(phi_amr)*sin(theta_amr) > 1d-5) print*, 'y est faux' !NB : y est faux pour 1d-6
     if (z_i - radius_amr_1*cos(theta_amr) > 1d-5) print*, 'z est faux'


     !print*, x_i, y_i, z_i, radius_amr_1, radius_amr_2, theta_amr, phi_amr

     alpha_solid = atan2(sqrt(3.d0)/2.d0*cell_size(i),radius_amr_1)
     if (alpha_solid < 0) alpha_solid = alpha_solid + 2.d0*pi

     !print*, x_i, y_i, z_i, radius_amr_1, cell_size(i), alpha_solid, '\n'

     !print*, 'alpha solide ', alpha_solid

     !phi_solid_1 = phi_amr - alpha_solid  !encadrement des angles de la cellule avec le demi-angle d'ouverture du cone
     !phi_solid_2 = phi_amr + alpha_solid
     !theta_solid_1 = theta_amr - alpha_solid
     !theta_solid_2 = theta_amr + alpha_solid



     call CPU_TIME(time_loop)
     if(time_loop - time_before > 25.d0) then
        write(*,*) real(i)/real(number_of_leaves)*100, '% proceeded...'
        time_before = time_loop
     endif

     !calcul de la masse totale : M = rho*cell_size**3 /!\ rho en H/cc et cell_size en kpc...
     ! 1 cm = 3.24d-22 kpc
     ! M_H = 1.67d-27 kg
     ! M_sun = 2.0d30 kg
     ! 1 M_sun/pc^3 = 1/40 H/cc  !! 1/30 avec corrections ?
     M_tot = M_tot + rho(i)/30.d0*(cell_size(i)*1000.d0)**3 ! en M_sun

     do k = 1, n_LOP !loop over n_LOP LOPs
        !print*, "LOP number ", k

        in_cell_i(k) = .false.
        x_tmp = 0 ; y_tmp = 0 ; z_tmp = 0 ; count_LOP = 0

        !on ne veut remplir chaque point de chaque LOP qu'une fois.
        do t = 0, t_max+1 !t_max_max+1
           bool_t(t,k) = 0 !le point t de la LOP k est associe a 0 cellules
        enddo

        !si la LOP passe par la cellule, alors scanner la LOP

        !angle solide
        !if (( theta_LOP(k) >= theta_amr_1 .and. theta_LOP(k) <= theta_amr_2 .and. phi_LOP(k) >= phi_amr_1 .and. phi_LOP(k) <= phi_amr_2) &
        if  ( ((abs(theta_LOP(k) - theta_amr) <= 1.05d0*alpha_solid) .and. (abs(phi_LOP(k) - phi_amr) <= 1.05d0*alpha_solid)) &
             & .or. (abs(x_i) <= sqrt(3.d0)*cell_size(i)/2.d0) &
             & .or. (abs(y_i) <= sqrt(3.d0)*cell_size(i)/2.d0) &
             & .or. (abs(z_i) <= sqrt(3.d0)*cell_size(i)/2.d0) ) then

           !uvw
           !xbool = x(i) >= x_center
           !ybool = y(i) >= y_center
           !zbool = z(i) >= z_center

           !les cellules a cheval sur deux octants ne sont pas prises en compte !
           !si le centre est dans un octant et que la LOP passe dans la cellule, mais dans un autre
           !octant, on ne la voit pas ---> tjs prendre en compte les cellules a cheval
           !if ((u(k) >= 0.d0 .and. xbool) .or. (u(k) <= 0.d0 .and. x(i) <= x_center) .or. abs(x_i) <= cell_size(i)/2.d0) then
           !if ((v(k) >= 0.d0 .and. ybool) .or. (v(k) <= 0.d0 .and. y(i) <= y_center) .or. abs(y_i) <= cell_size(i)/2.d0) then
           !if ((w(k) >= 0.d0 .and. zbool) .or. (w(k) <= 0.d0 .and. z(i) <= z_center) .or. abs(z_i) <= cell_size(i)/2.d0) then  !----> CA MARCHE !!!

           do t = 0, t_max+1 !loop over t_max points belonging to LOP number k

              x_LOP = x_center + t*dx(k)
              y_LOP = y_center + t*dy(k)
              z_LOP = z_center + t*dz(k)
              !write(*,*) t, t_max, x_LOP, y_LOP, z_LOP, x(i), y(i), z(i)

              in_condition = ( x_LOP >= (x(i) - cell_size(i)/2.d0) ) .and. ( x_LOP < (x(i) + cell_size(i)*(.5d0) ) ) .and. &
                   &     ( y_LOP >= (y(i) - cell_size(i)/2.d0) ) .and. ( y_LOP < (y(i) + cell_size(i)*(.5d0) ) ) .and. &
                   &     ( z_LOP >= (z(i) - cell_size(i)/2.d0) ) .and. ( z_LOP < (z(i) + cell_size(i)*(.5d0) ) ) .and. &
                   &     ( bool_t(t,k) == 0 ) .and. ( i /= number_of_leaves+1 )
              !si le point de la LOP est disponible !ignorer le point fantome

              !si le point de la LOP k est dans la cellule i, on ecrit tous les parametres
              if (in_condition)  then

                 !print*, 'point de la ligne de visee', k, ' dans la cellule ', i, 'position : ', x_LOP, y_LOP, z_LOP
                 x_tmp = x_tmp + x_LOP
                 y_tmp = y_tmp + y_LOP
                 z_tmp = z_tmp + z_LOP
                 count_LOP = count_LOP + 1
                 in_cell_i(k) = .true.
                 bool_t(t,k) = 1 !le point de la LOP est compte dans une cellule : on ne doit plus l'utiliser
                 !attention, ce n'est pas le nombre de points occupes sur la LOP car on garde un seul point par cellule  !!

                 !dans les grandes cellules, il peut y avoir plusieurs points de la LOP. on ne veut en garder qu'1 et faire la moyenne
                 !de leurs positions. (les autres parametres sont les memes)
              endif !end of condition "point t of LOP k is in cell i"

           enddo !end of loop "scanning all points in LOP number k"

           if(.not. in_condition .and. in_cell_i(k)) then
              !la LOP a ete parcourue entierement : on fait la moyenne des positions des points de la LOP k contenus dans la cellule i
              x_tmp = x_tmp/count_LOP
              y_tmp = y_tmp/count_LOP
              z_tmp = z_tmp/count_LOP
              !print*, 'average position :', x_tmp, y_tmp, z_tmp, count_LOP
              depth = sqrt( (x_tmp - x_center)**2 + (y_tmp - y_center)**2 + (z_tmp - z_center)**2 ) !real(t)/real(t_max)*L ! !kpc, rho in H/cc

              !a cause du moyennage, ces angles peuvent legerement varier par rapport aux originaux
              theta_tmp = acos((z_tmp - z_center)/depth)
              !if (y_tmp >= 0.d0) phi_tmp = acos((x_tmp - x_center)/sqrt( (x_tmp - x_center)**2 + (y_tmp - y_center)**2 ))
              !if (y_tmp  < 0.d0) phi_tmp = 2.d0*pi - acos((x_tmp - x_center)/sqrt( (x_tmp - x_center)**2 + (y_tmp - y_center)**2 ))
              phi_tmp = atan2((y_tmp - y_center),(x_tmp - x_center))
              if (phi_tmp < 0) phi_tmp = phi_tmp + 2.d0*pi

              !k_write = k + 30
              write(k + 30,'(15d20.12)') depth, rho(i), x(i), y(i), z(i), temp(i), vx(i), vy(i), vz(i), &
                   & cell_size(i), x_tmp, y_tmp, z_tmp, theta_tmp, phi_tmp
              !print *,rho(i)
              !write(*,*) 'hello !'
              count_points(k) = count_points(k) + 1
              x_tmp = 0 ; y_tmp = 0 ; z_tmp = 0 ; count_LOP = 0
           endif !end of condition "write average parameters of cell i for each LOP k points"

        endif !angle solide

        !endif
        !endif
        !else
        !print*, 'aie aie aie !!'
        !print*, u(k), v(k), w(k), x(i)-x_center, y(i)-y_center, z(i)-z_center
        !endif !uvw



     enddo !end of loop "scanning all n_LOP LOPs"

  enddo !enod of loop "scanning all cells"

  !do k = 1, n_LOP
  !do t = 0, t_max_max+1
  !print*, bool_t(t,k)
  !enddo
  !enddo

  close(11)
  close(12)
  close(13)
  close(14)

  write(*,*) 'Generated file : ~/LOPs'//trim(sim)//&
       &'/LOP_plane_'//trim(sim)//'_'//trim(half)//'.ascii'

  do k = 1, n_LOP
     close(k+30)
     write(char_k,'(I5)') k
     char_k = adjustl(char_k)
     print*, "Il y a ", count_points(k), " points sur la LOP ", k
     write(*,*) 'Generated file : ~/LOPs'//trim(sim)//&
          &'/density_profile_'//trim(sim)//'_LOP'&
          &//trim(char_k)//trim(half)//'__*date_time*.ascii'

  enddo

  write(*,*) "Masse totale : ", M_tot, " M_sun"

  deallocate(x,y,z,vx,vy,vz,temp,rho,cell_size,ilevel,bool_t,in_cell_i,&
       &count_points,x_border,y_border,z_border,theta_LOP,phi_LOP,u,v,w)

  call CPU_TIME(time2)
  write(*,*) 'Execution time : ', time2-time1, ' s'

  print*, 'la taille des plus petites cellules est : ', test_min
  print*, 'elle devrait etre 50 kpc / 2^l_max, ie : ', cell_size_min


end program line_of_prop_cone




!commande a taper pour faire tourner amr2map (version modifiee par moi)
!dans une zone restreinte de la boite contenant quand meme toute la galaxie                             " " == statistics_00100.d0ascii et gas_part_00100.d0ascii

!amr2map -inp output_00100/ -xmin 0.15d0 -xmax 0.85d0 -ymin 0.15d0 -ymax 0.85d0 -zmin 0.15d0 -zmax 0.85d0 -lmax 13 -ascii ---> " ".lmax13 et dossier LOPs


!cartes : tranches de ~ 50 pc (densite)
!amr2map -inp output_00100/ -dir x -xmin 0.4995d0 -xmax 0.5005d0 -out central_x_thinslice_lmax13 -ymin 0.15d0 -ymax 0.85d0 -zmin 0.15d0 -zmax 0.85d0 -lmax 13 -typ 1
!amr2map -inp output_00100/ -dir y -ymin 0.4995d0 -ymax 0.5005d0 -out central_y_thinslice_lmax13 -xmin 0.15d0 -xmax 0.85d0 -zmin 0.15d0 -zmax 0.85d0 -lmax 13 -typ 1
!amr2map -inp output_00100/ -dir z -zmin 0.4995d0 -zmax 0.5005d0 -out central_z_thinslice_lmax13 -xmin 0.15d0 -xmax 0.85d0 -ymin 0.15d0 -ymax 0.85d0 -lmax 13 -typ 1

