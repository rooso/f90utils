#############################################################################

#############################################################################
#############################################################################

# Fortran compiler options and directives

SYSTEM="curie"
###SYSTEM="mac"

#--------- Compiler -----------------
ifeq ($(SYSTEM),"curie")
F90 = ifort
F_OPTIONS = -ggdb -fpp -free 
FITS_LIB = cfitsio
LIBDIR = /ccc/cont003/home/dsm/rooso/cfitsio/lib/
###LIBDIR = /ccc/cont005/home/gen2192/gaborj/src/cfitsio/
###FFLAGS = -x f95-cpp-input $(DEFINES) -ggdb
endif

ifeq ($(SYSTEM),"mac")
###F90 = gfortran
F90 = mpif90
F_OPTIONS = -g -ffree-line-length-none -cpp
FITS_LIB = cfitsio
LIBDIR = /sw/lib/
endif

FFT = ###FFT
DEFINES = -D$(FFT)

#############################################################################
SOURCE_DIR = .
BIN_DIR = ./bin
#############################################################################

FFT_LIBDIR = /usr/local/fftw3-3.3.1/lib/
FFT_INCLUDE = /usr/local/fftw3-3.3.1/include/
FFT_OPTS =

ifdef $(FFT)
FFT_OPTS = -L$(FFT_LIBDIR) -lfftw3 -lm -I$(FFT_INCLUDE)
endif

amr2cubeRS : 
	$(F90)  $(F_OPTIONS)    $(SOURCE_DIR)/amr2cubeRS.f90    -o  $(BIN_DIR)/amr2cubeRS   -L$(LIBDIR)  -l$(FITS_LIB)   $(FFT_OPTS)
amr2map : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/amr2map.f90 -o $(BIN_DIR)/amr2map -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
part2map : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/part2map.f90 -o $(BIN_DIR)/part2map -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
kinematic : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/kinematic.f90 -o $(BIN_DIR)/kinematic -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
logfile_extract : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/logfile_extract.f90 -o $(BIN_DIR)/logfile_extract
outflows : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/outflows.f90 -o $(BIN_DIR)/outflows -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
inflows : 
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/inflows.f90 -o $(BIN_DIR)/inflows -L$(LIBDIR)
sfr:
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/sfr_part.f90 -o $(BIN_DIR)/sfr_part -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
	$(F90) $(F_OPTIONS) $(SOURCE_DIR)/sfr_gas.f90 -o $(BIN_DIR)/sfr_gas -L$(LIBDIR) -l$(FITS_LIB) $(FFT_OPTS)
clean : rm -f *.o *.$(MOD)
new : clean amr2map
all : amr2map part2map

