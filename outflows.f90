program outflows

  !! conversion of amr2map.f90 to measure outflows
  !! Procedure:  Choose several planes parallel to the x-y plane through 
  !! which to measure the mass outflow rate.  For each cell in the box,
  !! determine how much of its mass (if any) will cross the plane within
  !! a set time dt_outflow, based on that cell's mass, position, and velocity.
  !!
  !! Modified by O. Roos on Oct 7th 2015 : 
  !! Choose several planes parallel to the x-y plane 
  !! OR concentric shells (with special care close to the disk, bec. do not want
  !to cross it) 
  !! through which to measure the mass outflow rate
  !!(NB : ifort compilation pb : truncates lines with > 70 characters since last
  !curie maintenance... annoying)

  !! Convert Ramses output on AMR/hydro into a fits
  !! Can also convert hydro data to an ascii file and/or compute the pdf
  !! Based on Romain Teyssier's amr2map
  !! Florent Renaud - 5 Aug 2011

  implicit none
#ifdef FFT
  include "fftw3.f" 
#endif
  integer::i, j, k, n, impi, icpu, ilevel, iidim, ivar, ind, ipdf
  character(len=5)::nchar,ncharcpu
  character(len=128)::filename, repository, suffix='', outval, outvalunit
  real(KIND=8)::xmin=0, xmax=1, ymin=0, ymax=1, zmin=0, zmax=1
  character(len=1)::dir
  integer::lmax=0, typ=1, pdfn=500
  logical::maxval=.false., ascii=.false., pdf=.false., makemap=.true., &
       maxrho=.false., psd=.false., maxall=.false.

  integer,parameter::seed=9876543
  real(KIND=8)::rx,ry,rz
  real(kind=8)::partmass=0.0
  integer::npartcell

  real(kind=8)::pdfmin=1.0D-3, pdfmax=5.0D5, lpdfampli, lpdfmin, &
       lpdfmax, threshold=0D0
  real(kind=8),dimension(:),allocatable::pdfhist

  integer*8 plan_forward
  integer::ipsd, jpsd, binpsd, psdmap
  character(len=2)::charlmax
  real(kind=8)::fpsd, psdnorm
  real(kind=8),dimension(:),allocatable::ppsd
  complex(kind=8),dimension(:,:),allocatable::psdout, psdin

  integer::ncpu, ndim, nx, ny, nz, nlevelmax, ngridmax, nboundary, ngrid_current
  integer::twotondim, levelmin, bit_length, maxdom, ndom, ncpu_read, nvarh
  integer::ngrida
  integer::idim, jdim, kdim, imin, imax, jmin, jmax, kmin, kmax
  integer::ix, iy, iz
  integer::nx_full, ny_full, nz_full
  integer::nx_sample=0, ny_sample=0
  real(kind=8)::xxmin, xxmax, yymin, yymax, zzmin, zzmax
  real(kind=8)::dkey, & !!! JMG order_min, 
       dx, dmax, dxline, boxlen, t, ljeans, mass
  real(kind=8), dimension(1) :: order_min
  real::weight
  character(len=80)::ordering
  logical::ok

  integer,dimension(1:8)::idom, jdom, kdom, cpu_min, cpu_max
  real(kind=8),dimension(1:3)::xbound
  real(kind=8),dimension(1:8,1:3)::xc
  integer,dimension(:,:),allocatable::ngridfile, ngridlevel, ngridbound
  integer,dimension(:),allocatable::cpu_list
  real(kind=8),dimension(:),allocatable::bound_key
  real(kind=8),dimension(1:8)::bounding_min, bounding_max
  logical,dimension(:),allocatable::cpu_read

  real(kind=8),dimension(:,:),allocatable::x, xg
  real(kind=8),dimension(:,:,:),allocatable::var
  real(kind=8),dimension(:),allocatable::rho, map
  logical,dimension(:),allocatable::ref
  integer,dimension(:,:),allocatable::son

  type level
     integer::ilevel
     integer::ngrid
     real(KIND=8),dimension(:,:),pointer::map
     real(KIND=8),dimension(:,:),pointer::rho
     integer::imin
     integer::imax
     integer::jmin
     integer::jmax
     integer::kmin
     integer::kmax
  end type level

  type(level),dimension(1:100)::grid

  integer::status, unit, blocksize, bitpix, naxis
  integer,dimension(2)::naxes
  integer::group,fpixel,nelements
  integer,dimension(300,200)::array
  logical::simple,extend
  character(2)::num


  real(KIND=4),dimension(:,:),allocatable::tmpmap
  integer::nxmap, nymap
  real,dimension(:,:),allocatable::map2

  real(kind=8)::scale_nH,scale_vkms,scale_T2,scale_t,scale_l,scale_d


  ! JMG -- variables for rho-T diagram
  logical :: rho_T=.false.
  real(kind=8)::Tmin=1.0d1, Tmax=1.0d9, lTmin, lTmax, lTbinsize
  real(kind=8), dimension(:, :), allocatable :: rho_T_map
  integer :: nbins_T=100, jTemp
  real(kind=8) :: cellTemp, cellDens

  ! 9 fields: 3 position, 3 velocity, density, temperature, level
  real(kind=8), dimension(9) :: cell, maxtemp_cell=0, &
       maxspeed_cell=0, maxdens_cell=0

  ! Variables for outflows
  real(kind=8) :: dx_loc, dx_pix, cell_size, vol_loc
  real(kind=8) :: xcell, ycell, zcell, rho_cell, &
       vel_cell, dA_cell, mcell, vcell_z
  real(kind=8) :: this_outflow
  integer(kind=8), parameter :: MAX_N_SURF=100
  real(kind=8), dimension(MAX_N_SURF) :: z_surf, z_surf_in
  real(kind=8) :: z_upper_0, z_lower_0, z_upper_1, z_lower_1
  integer(kind=8) :: ind_surf, n_surf
  real(kind=8) :: f_above_0, f_below_0, f_above_1, f_below_1
  real(kind=8) :: fcross_up, fcross_down
  real(kind=8) :: mass_flux_up(MAX_N_SURF), mass_flux_down(MAX_N_SURF)
  real(kind=8) :: dt_outflow, dt_outflow_in=1.0


  !OR -- variables for concentric shells
  logical :: shells=.false., box_intersects_shell=.false.
  logical :: box_intersects_shell_dt=.false.
  real(kind=8), dimension(MAX_N_SURF) :: R_shell, R_shell_in
  real(kind=8)::center_coord, vcell_x, vcell_y, Rcell
  integer(kind=8):: ind_shell, n_shell
  real(kind=8)::min_thickness=2.5, pi
  real(kind=8)::f_inside_0, f_outside_0, f_inside_1, f_outside_1
  real(kind=8)::fcross_in, fcross_out
  integer(kind=8), parameter :: n_iterations=1 !1d3
  real(kind=8) :: rand(n_iterations,3,2)
  real(kind=8)::x_MC, y_MC, z_MC
  integer(kind=8)::n_intersection_1,n_intersection_0, ind_MC
  real(kind=8) :: mass_flux_out(MAX_N_SURF), mass_flux_in(MAX_N_SURF)
  real(kind=8),dimension(3)::Center,Cell_min,Cell_max
  real(kind=8),dimension(3)::Cell_min_dt,Cell_max_dt
  !

  real(kind=8) :: scale_m, scale_m_Msun


  ! DEBUG
  integer, dimension(256) :: junkarr
  !=======================================================================  

  call read_params

  ascii = .true.

  ! count # of surfaces 
  n_surf=0
  do ind_surf=1, MAX_N_SURF
     if(z_surf_in(ind_surf) > 0.0) n_surf = n_surf+1
  enddo

  n_shell=0
  do ind_shell=1, MAX_N_SURF
     if(R_shell_in(ind_shell) > 0.0) n_shell = n_shell+1
  enddo
  if (n_shell .gt. 0) shells = .true.

  if(ascii) makemap=.false.

  ! Read hydro data
  i=INDEX(repository,'output_')
  nchar=repository(i+7:i+13)

  filename=TRIM(repository)//'/hydro_'//TRIM(nchar)//'.out00001'
  inquire(file=filename, exist=ok)
  if(.not. ok)then
     write(*,*) "Error: ", trim(filename), " not found"
     stop
  endif

  filename=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'
  inquire(file=filename, exist=ok)
  if(.not. ok)then
     write(*,*) "Error: ", trim(filename), " not found"
     stop
  endif

  filename=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out00001'
  open(unit=1, file=filename, status='old', form='unformatted')
  read(1) ncpu
  read(1) ndim
  read(1) nx, ny, nz
  read(1) nlevelmax
  read(1) ngridmax
  read(1) nboundary
  read(1) ngrid_current
  read(1) boxlen
  close(1)

  !OR
  pi = 4.d0*atan(1.d0) !pi
  center_coord = boxlen/2.d0
  Center(1) = center_coord
  Center(2) = center_coord
  Center(3) = center_coord
  !

  twotondim=2**ndim
  xbound=(/dble(nx/2),dble(ny/2),dble(nz/2)/)

  allocate(ngridfile(1:ncpu+nboundary,1:nlevelmax))
  allocate(ngridlevel(1:ncpu,1:nlevelmax))
  if(nboundary > 0) allocate(ngridbound(1:nboundary,1:nlevelmax))

  filename=TRIM(repository)//'/info_'//TRIM(nchar)//'.txt'
  inquire(file=filename, exist=ok)
  if(.not. ok)then
     write(*,*) "Error: ", trim(filename), " not found"
     stop
  endif

  open(unit=1, file=filename, form='formatted', status='old')
  read(1,*)
  read(1,*)
  ! JMG  read(1,'("levelmin    =",I11)')levelmin
  read(1,'(13X,I11)')levelmin
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  ! JMG   read(1,'("time        =",E23.15)')t
  read(1,'(13X,E23.15)')t
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  !JMG
  !  read(1,'("unit_l      =",E23.15)')scale_l
  !  read(1,'("unit_d      =",E23.15)')scale_d
  !  read(1,'("unit_t      =",E23.15)')scale_t
  read(1,'(13X,E23.15)')scale_l
  read(1,'(13X,E23.15)')scale_d
  read(1,'(13X,E23.15)')scale_t
  read(1,*)
  ! JMG  read(1,'("ordering type=",A80)'),ordering
  read(1,'(14X,A80)'),ordering
  read(1,*)

  ! DEBUG -->  WRITE(*,*) "Levelmin:", levelmin

  !kB      = 1.3806200d-16
  !clight  = 2.9979250d+10
  !Gyr     = 3.1536000d+16
  !X       = 0.76
  !mH      = 1.6600000d-24
  !  output Ramses Merger: kpc, 1e9 Msun
  ! conversion to CGS:
  !  scale_l  = 3.08567752D21  ! = 1 kpc                   in cgs
  !  scale_m  = 1.9889D42      ! = 1e9 Msun                in cgs
  !  scale_d  = 6.77025D-23    ! = 1e9 Msun / kpc^3        in cgs
  !  scale_t  = 4.7043D14      ! = ramses time unit        in cgs
  !  scale_v  = 6.559269D6     ! = ramses velocity unit    in cgs
  ! conversion to useful units: multiply the Ramses output with the scale_X
  !  scale_nH   = 30.996345 ! = ramses density     in H/cc
  !  scale_vkms = 65.59269  ! = ramses velocity    in km/s
  !  scale_T2   = 5.17302D5 ! = ramses temperature in Kelvin
  !  scale_t    = 14.9070   ! = ramses time        in Myr

  scale_m = scale_d * scale_l**3
  scale_m_Msun = scale_m / 1.989d33
  scale_T2 = (scale_l / scale_t)**2 * 1.66D-24 / 1.3806200D-16
  scale_vkms = scale_l / scale_t / 1D5
  scale_nH = scale_d / 1.66D-24 * 0.76
  scale_l = scale_l / 3.085677581282D21
  scale_t = scale_t / 3.15576D13


  ! temperature * scale_T2 = K
  ! velocity * scale_vkms = km / s
  ! density * scale_nH = H/cm^3
  ! length * scale_l = kpc
  ! time * scale_t = Myr

  ! convert outflow parameters to simulation units
  ! from Myr to code units
  dt_outflow = dt_outflow_in / scale_t
  if (.not. shells) then
     do ind_surf=1, n_surf
        ! from kpc to code units, which are usually kpc
        z_surf(ind_surf) = z_surf_in(ind_surf) / scale_l
     end do
  else
     do ind_shell=1, n_shell
        ! from kpc to code units, which are usually kpc
        R_shell(ind_shell) = R_shell_in(ind_shell) / scale_l
     end do
  endif

  t = t * scale_t
  threshold = threshold / scale_nH  

  allocate(cpu_list(1:ncpu))
  if(TRIM(ordering).eq.'hilbert')then
     allocate(bound_key(0:ncpu))
     allocate(cpu_read(1:ncpu))
     cpu_read=.false.
     do impi=1,ncpu
        read(1,'(I8,1X,E23.15,1X,E23.15)') i, bound_key(impi-1), bound_key(impi)
     end do
  endif
  close(1)

  ! Compute map parameters
  if(lmax==0) lmax=nlevelmax
  ! ADDED BY JMG to prevent annoying bug
  if(lmax > nlevelmax) lmax = nlevelmax
  !JMG


  !  write(*,*)'time=',t
  !  write(*,*)'Working resolution =',2**lmax

  if(ndim>2)then  
     select case (dir)
     case ('x')
        idim=3
        jdim=2
        kdim=1
        xxmin=zmin ; xxmax=zmax
        yymin=ymin ; yymax=ymax
        zzmin=xmin ; zzmax=xmax
     case ('y')
        idim=1
        jdim=3
        kdim=2
        xxmin=xmin ; xxmax=xmax
        yymin=zmin ; yymax=zmax
        zzmin=ymin ; zzmax=ymax
     case default
        idim=1
        jdim=2
        kdim=3
        xxmin=xmin ; xxmax=xmax
        yymin=ymin ; yymax=ymax
        zzmin=zmin ; zzmax=zmax
     end select

  else
     idim=1
     jdim=2
     xxmin=xmin ; xxmax=xmax
     yymin=ymin ; yymax=ymax          
     ! needed ?
     zzmin=0.0  ; zzmax=1.0
  end if

  !   xmin_image = xxmin * boxlen
  !   ymin_image = yymin * boxlen
  !!  imsize = (xxmax - xxmin) * boxlen
  !!  WRITE(*,*) 'JUNK1', xmin_image, ymin_image, imsize


  if(TRIM(ordering).eq.'hilbert')then
     dmax=max(xmax-xmin,ymax-ymin,zmax-zmin)
     do ilevel=1,lmax
        dx=0.5d0**ilevel
        if(dx.lt.dmax) exit
     end do

     bit_length=ilevel-1
     maxdom=2**bit_length
     imin=0; imax=0; jmin=0; jmax=0; kmin=0; kmax=0
     if(bit_length>0)then
        imin=int(xmin*dble(maxdom))
        imax=imin+1
        jmin=int(ymin*dble(maxdom))
        jmax=jmin+1
        kmin=int(zmin*dble(maxdom))
        kmax=kmin+1
     end if

     dkey=(dble(2**(nlevelmax+1)/dble(maxdom)))**ndim

     ndom=1
     if(bit_length>0)ndom=8
     idom(1)=imin; idom(2)=imax
     idom(3)=imin; idom(4)=imax
     idom(5)=imin; idom(6)=imax
     idom(7)=imin; idom(8)=imax
     jdom(1)=jmin; jdom(2)=jmin
     jdom(3)=jmax; jdom(4)=jmax
     jdom(5)=jmin; jdom(6)=jmin
     jdom(7)=jmax; jdom(8)=jmax
     kdom(1)=kmin; kdom(2)=kmin
     kdom(3)=kmin; kdom(4)=kmin
     kdom(5)=kmax; kdom(6)=kmax
     kdom(7)=kmax; kdom(8)=kmax

     do i=1,ndom
        if(bit_length>0)then
           call hilbert3d(idom(i),jdom(i),kdom(i),order_min,bit_length,1)
        else
           order_min=0.0d0
        endif
        bounding_min(i)=(order_min(1))*dkey
        bounding_max(i)=(order_min(1)+1.0D0)*dkey
     end do

     cpu_min=0
     cpu_max=0
     do impi=1,ncpu
        do i=1,ndom
           if (bound_key(impi-1).le.bounding_min(i) &
                .and.bound_key(impi).gt.bounding_min(i)) cpu_min(i)=impi
           if (bound_key(impi-1).lt.bounding_max(i) &
                .and.bound_key(impi).ge.bounding_max(i)) cpu_max(i)=impi
        end do
     end do

     ncpu_read=0
     do i=1,ndom
        do j=cpu_min(i), cpu_max(i)
           if(.not. cpu_read(j))then
              ncpu_read=ncpu_read+1
              cpu_list(ncpu_read)=j
              cpu_read(j)=.true.
           endif
        enddo
     enddo
  else
     ncpu_read=ncpu
     do j=1,ncpu
        cpu_list(j)=j
     end do
  end  if
  ! end if on hilbert

  ! Compute hierarchy
  do ilevel=1,lmax
     nx_full=2**ilevel
     ny_full=nx_full
     nz_full=nx_full
     imin=int(xxmin*dble(nx_full))+1
     imax=int(xxmax*dble(nx_full))+1
     jmin=int(yymin*dble(ny_full))+1
     jmax=int(yymax*dble(ny_full))+1
     allocate(grid(ilevel)%map(imin:imax,jmin:jmax))
     allocate(grid(ilevel)%rho(imin:imax,jmin:jmax))
     grid(ilevel)%map(:,:)=0.0
     grid(ilevel)%rho(:,:)=0.0
     grid(ilevel)%imin=imin
     grid(ilevel)%imax=imax
     grid(ilevel)%jmin=jmin
     grid(ilevel)%jmax=jmax    
     grid(ilevel)%kmin=int(zzmin*dble(nz_full))+1
     grid(ilevel)%kmax=int(zzmax*dble(nz_full))+1
  end do

  ! Compute projected variables

  ! Loop over cpu files
  do k=1,ncpu_read
     !OR
     write(*,*) 'CPU ', k, ' of ', ncpu_read, '(', k*1d0/ncpu_read*100, ' %)'
     !
     icpu=cpu_list(k)
     write(ncharcpu,'(I5.5)') icpu

     ! Open AMR file and skip header
     filename=TRIM(repository)//'/amr_'//TRIM(nchar)//'.out'//TRIM(ncharcpu)
     open(unit=1, file=filename, status='old', form='unformatted')
     !    write(*,*)'Processing file '//TRIM(filename)
     do i=1,21
        read(1)
     end do
     ! Read grid numbers
     read(1) ngridlevel
     ngridfile(1:ncpu,1:nlevelmax)=ngridlevel
     read(1)
     if(nboundary>0)then
        read(1)
        read(1)
        read(1) ngridbound
        ngridfile(ncpu+1:ncpu+nboundary,1:nlevelmax)=ngridbound
     endif
     read(1)     
     read(1) ! comment this line for old stuff
     if(TRIM(ordering).eq.'bisection')then
        do i=1,5
           read(1)
        end do
     else
        read(1)
     endif
     read(1)
     read(1)
     read(1)

     ! Open HYDRO file and skip header
     open(unit=2, file=TRIM(repository)//'/hydro_'//TRIM(nchar)&
          //'.out'//TRIM(ncharcpu), status='old', form='unformatted')
     read(2)
     read(2) nvarh
     read(2)
     read(2)
     read(2)
     read(2)

     ! Loop over levels
     do ilevel=1, lmax
        ! Geometry
        dx=0.5**ilevel
        dx_loc = dx * boxlen
        vol_loc = dx_loc**3  ! in simulation units
        dxline=1
        if(ndim==3) dxline=dx
        nx_full=2**ilevel
        ny_full=2**ilevel
        nz_full=2**ilevel

        do ind=1,twotondim
           iz=(ind-1)/4
           iy=(ind-1-4*iz)/2
           ix=(ind-1-2*iy-4*iz)
           xc(ind,1)=(dble(ix)-0.5D0)*dx
           xc(ind,2)=(dble(iy)-0.5D0)*dx
           xc(ind,3)=(dble(iz)-0.5D0)*dx
        end do

        ! Allocate work arrays
        ngrida=ngridfile(icpu,ilevel)
        grid(ilevel)%ngrid=ngrida
        if(ngrida>0)then
           allocate(xg(1:ngrida,1:ndim))
           allocate(son(1:ngrida,1:twotondim))
           allocate(var(1:ngrida,1:twotondim,1:nvarh))
           allocate(x  (1:ngrida,1:ndim))
           allocate(rho(1:ngrida))
           allocate(map(1:ngrida))
           allocate(ref(1:ngrida))
        endif

        ! Loop over domains
        do j=1,nboundary+ncpu
           ! Read AMR data
           if(ngridfile(j,ilevel)>0)then
              read(1) ! Skip grid index
              read(1) ! Skip next index
              read(1) ! Skip prev index
              ! Read grid center
              do iidim=1,ndim
                 if(j.eq.icpu)then
                    read(1) xg(:,iidim)
                 else
                    read(1)
                 endif
              end do
              read(1) ! Skip father index
              do ind=1,2*ndim
                 read(1) ! Skip nbor index
              end do
              ! Read son index
              do ind=1,twotondim
                 if(j.eq.icpu)then
                    read(1) son(:,ind)
                 else
                    read(1)
                 end if
              end do
              ! Skip cpu map
              do ind=1,twotondim
                 read(1)
              end do
              ! Skip refinement map
              do ind=1,twotondim
                 read(1)
              end do
           endif

           ! Read hydro data
           read(2)
           read(2)
           if(ngridfile(j,ilevel)>0)then
              ! Read hydro variables
              do ind=1,twotondim
                 do ivar=1,nvarh
                    if(j.eq.icpu)then
                       read(2) var(:,ind,ivar)
                    else
                       read(2)
                    end if
                 end do
              end do
           end if
        end do
        ! end loop over domains

        ! Compute map
        if(ngrida>0)then

           ! Loop over cells
           do ind=1,twotondim
              ! Compute cell center
              do i=1,ngrida
                 x(i,1)=(xg(i,1)+xc(ind,1)-xbound(1))
                 x(i,2)=(xg(i,2)+xc(ind,2)-xbound(2))
                 if(ndim>2)x(i,3)=(xg(i,3)+xc(ind,3)-xbound(3))
              end do
              ! Check if cell is refined
              do i=1,ngrida
                 ref(i)=son(i,ind)>0.and.ilevel<lmax
              end do
              ! Extract variable

              ! var(i,1): d, var(i,2:ndim+1): u,v,w and var(i,ndim+2): P.
              rho = var(:,ind,1)

              ! Loop over grids
              do i=1,ngrida

                 ! check if this cell is refined -- is it a leaf cell?
                 if(.not.ref(i))then

                    ! place this cell within a map pixel
                    !               ix=int(x(i,idim)*dble(nx_full))+1
                    !               iy=int(x(i,jdim)*dble(ny_full))+1
                    !               iz=int(x(i,kdim)*dble(nz_full))+1

                    ! Selection
                    ! does this cell live within the ranges specified by user?
!!!JMG 
                    !!               write(*,*) x(i,1:3), xxmin, xxmax, yymin, yymax, zzmin, zzmax
                    if( x(i,idim)>=xxmin .and. x(i,idim)<=xxmax .and. &
                         x(i,jdim)>=yymin .and. x(i,jdim)<=yymax .and. &
                         x(i,kdim)>=zzmin .and. x(i,kdim)<=zzmax ) then 

                       !!                  write(*,*) "Made it"
!!!JMG
                       !!
                       !if(ix>=grid(ilevel)%imin.and.iy>=grid(ilevel)%jmin.and.ix<=grid(ilevel)%imax.and.iy<=grid(ilevel)%jmax.and.iz>=grid(ilevel)%kmin.and.iz<=grid(ilevel)%kmax)
                       !then  

                       ! Get gas cell position
                       xcell=x(i,1)*boxlen
                       ycell=x(i,2)*boxlen
                       zcell=x(i,3)*boxlen
                       cell_size = dx_loc

                       if (shells) then
                          vcell_x = var(i,ind,2)
                          vcell_y = var(i,ind,3)
                       endif
                       vcell_z = var(i,ind,4)

                       rho_cell = rho(i)

                       !OR DEBUG !!!
                       !vcell_x = 0
                       !vcell_y = 0
                       !vcell_z = 1
                       !rho_cell = 1
!!!

                       ! total mass in this cell in simulation units
                       mcell = rho_cell * vol_loc
                       !mcell=1

                       ! Loop over surfaces
                       ! 
                       if (.not. shells) then
                          do ind_surf = 1, n_surf
                             ! upper and lower edges of the cell
                             z_upper_0 = zcell + 0.5 * cell_size
                             z_lower_0 = zcell - 0.5 * cell_size

                             ! upper and lower edges after moving at its velocity for a time dt_outflow
                             ! NB: dt_outflow must be in simulation units here
                             z_upper_1 = z_upper_0 + dt_outflow * vcell_z
                             z_lower_1 = z_upper_1 - cell_size

                             ! fraction of cell on each side of the surface at the start
                             f_above_0 = &
                                  (z_upper_0-MAX(z_lower_0,z_surf(ind_surf)))/cell_size
                             f_above_0 = MAX(f_above_0, 0.0)    ! must be >= 0
                             f_below_0 = 1.0 - f_above_0

                             ! fraction of cell on each side of the surface after moving for dt_outflow
                             f_above_1 = &
                                  (z_upper_1-MAX(z_lower_1,z_surf(ind_surf)))/cell_size
                             f_above_1 = MAX(f_above_1, 0.0)    ! must be >= 0
                             f_below_1 = 1.0 - f_above_1

                             ! Fraction of the cell that has moved across the line in each direction
                             ! Require to be > 0.0
                             fcross_up = MAX(0.0, f_above_1 - f_above_0)
                             fcross_down = MAX(0.0, f_below_1 - f_below_0)

                             mass_flux_up(ind_surf) = mass_flux_up(ind_surf) &
                                  + mcell * fcross_up
                             mass_flux_down(ind_surf) = mass_flux_down(ind_surf) &
                                  + mcell * fcross_down

                             if(fcross_up > 0 .or. fcross_down > 0) then 
                                !                        write(*,*) mcell*scale_m_Msun, z_upper_0, z_lower_0,
                                !                        cell_size, (vcell_z*dt_outflow)*scale_l, fcross_up,
                                !                        fcross_down
                                !                        write(*,*) "************************", z_upper_1,
                                !                        z_upper_0, z_surf(ind_surf)
                             endif
                          enddo     ! loop over outflow surfaces   

                       else !OR : if shells

                          n_intersection_0 = 0
                          n_intersection_1 = 0

                          Cell_min(1) = xcell - 0.5 * cell_size !lower corner of cell
                          Cell_min(2) = ycell - 0.5 * cell_size
                          Cell_min(3) = zcell - 0.5 * cell_size
                          Cell_max(1) = xcell + 0.5 * cell_size !upper corner of cell
                          Cell_max(2) = ycell + 0.5 * cell_size
                          Cell_max(3) = zcell + 0.5 * cell_size

                          Cell_min_dt(1) = Cell_min(1) + dt_outflow*vcell_x !lower corner after dt
                          Cell_min_dt(2) = Cell_min(2) + dt_outflow*vcell_y
                          Cell_min_dt(3) = Cell_min(3) + dt_outflow*vcell_z
                          Cell_max_dt(1) = Cell_max(1) + dt_outflow*vcell_x !upper corner after dt
                          Cell_max_dt(2) = Cell_max(2) + dt_outflow*vcell_y
                          Cell_max_dt(3) = Cell_max(3) + dt_outflow*vcell_z

                          !if cell far enough from disk
                          if (abs(zcell - center_coord) >= min_thickness) then
                             !OR
                             !!write(*,*) 'This cell is > 2.5 kpc from disk'
                             !

!!!WTF ?! Ifort on Curie truncates lines
                             ! with > col. 70 since last maintenance

                             do ind_shell = 1, n_shell !OR : loop over shells
                                !OR
                                !write(*,*) 'This is shell ', ind_shell, &
                                !          'of radius ', R_shell(ind_shell)
                                !

                                !do n=1,3
                                !write(*,*) Cell_min(n), Cell_max(n), Center(n)
                                !enddo
                                !write(*,*) R_shell(ind_shell)

                                f_inside_0 = 0.d0
                                f_inside_1 = 0.d0

                                if ((xcell - center_coord)**2 + (ycell - center_coord)**2 &
                                     + (zcell - center_coord)**2 .le. R_shell(ind_shell)**2) then
                                   f_inside_0 = 1.d0
                                endif

                                if ((xcell + dt_outflow*vcell_x - center_coord)**2 + &
                                     (ycell + dt_outflow*vcell_y - center_coord)**2 + &
                                     (zcell + dt_outflow*vcell_z - center_coord)**2 & 
                                     .le. R_shell(ind_shell)**2) then
                                   f_inside_1 = 1.d0
                                endif

                                !if (f_inside_0 == 1 .or f_inside_1 == 1) then
!!!!Does cell intersect shell ind_shell ?
!!!call Box_Sphere_Intersect(box_intersects_shell, &
!!!Cell_min, Cell_max, Center, R_shell(ind_shell),0)

!!!!Does cell intersect shell after dt ?
!!!call Box_Sphere_Intersect(box_intersects_shell_dt, &
!!!Cell_min_dt, Cell_max_dt, Center, R_shell(ind_shell),0)

!!!if(box_intersects_shell &
!!!.or. box_intersects_shell_dt) then

                                !write(*,*) 'Cell ', xcell, ycell, zcell, cell_size, &
                                !                     ' intersects shell ', ind_shell, ' !'
                                !write(*,*) 'or Cell+dt', xcell+dt_outflow*vcell_x, &
                                !ycell+dt_outflow*vcell_y, zcell+dt_outflow*vcell_z, cell_size, &
                                !                     ' intersects shell ', ind_shell, ' !'

                                !OR
                                !write(*,*) 'Begin MC'
                                !
!!!CALL RANDOM_NUMBER(rand)

!!!do ind_MC = 1, n_iterations

                                !DRAW MC POINTS IN CELL
!!!x_MC = Cell_min(1) + rand(ind_MC,1,1)*cell_size
!!!y_MC = Cell_min(2) + rand(ind_MC,2,1)*cell_size
!!!z_MC = Cell_min(3) + rand(ind_MC,3,1)*cell_size

                                !check if MC point is in shell
!!!if ((x_MC - center_coord)**2 + (y_MC - center_coord)**2 &
!!!+ (z_MC - center_coord)**2 .le. R_shell(ind_shell)**2) then
                                !write(*,*) 'This MC point is in the shell and in the cell'
!!!n_intersection_0 = n_intersection_0 + 1
!!!endif

                                !DRAW MC POINTS IN CELL at t+dt
!!!x_MC = Cell_min_dt(1) + rand(ind_MC,1,2)*cell_size
!!!y_MC = Cell_min_dt(2) + rand(ind_MC,2,2)*cell_size
!!!z_MC = Cell_min_dt(3) + rand(ind_MC,3,2)*cell_size

                                !check if MC point is still in shell at t+dt
!!!if ((x_MC - center_coord)**2 + (y_MC - center_coord)**2 &
!!!+ (z_MC - center_coord)**2 .le. R_shell(ind_shell)**2) then
                                !write(*,*) 'This MC point is still in the shell and in the cell'
!!!n_intersection_1 = n_intersection_1 + 1
!!!endif

!!!enddo !MC loop
                                !write(*,*) 'END MC'

                                !if (n_intersection_0 .ne. 0 .or. n_intersection_1 .ne. 0) then
                                !write(*,*) 'Shell number ',ind_shell,' radius ',R_shell_in(ind_shell),' kpc'
                                !write(*,*) 'Number of intersections at t_0 :', n_intersection_0
                                !write(*,*) 'Number of intersections at t_1 :', n_intersection_1
                                !endif

                                !n_iterations points are in the cell
                                !n_intersection_0/1 points are both in the cell and in the shell
!!!f_inside_0 = n_intersection_0*1d0/n_iterations
                                f_outside_0 = 1.0d0 - f_inside_0

!!!f_inside_1 = n_intersection_1*1d0/n_iterations
                                f_outside_1 = 1.0d0 - f_inside_1


                                ! Fraction of the cell that has moved across the sphere in each direction
                                ! (inward or outward)
                                ! Require to be > 0.0
                                fcross_out = MAX(0.0, f_outside_1 - f_outside_0)
                                fcross_in = MAX(0.0, f_inside_1 - f_inside_0)

                                mass_flux_out(ind_shell) = mass_flux_out(ind_shell) &
                                     + mcell * fcross_out
                                mass_flux_in(ind_shell) = mass_flux_in(ind_shell) &
                                     + mcell * fcross_in

                                !endif !if cell intersects shell

                             enddo !loop over shells
                          endif !if cell far enough from disk

                       endif !if: surfaces or shells
                       !

                    endif
                    ! end of Selection

                 end if
222              continue
              end do
           end do
           ! End loop over cell
           deallocate(xg, son, var, ref, rho, map, x)
        end if
     end do
     ! End loop over levels

     close(1)
     close(2)

  end do
  ! End loop over cpu

  ! instead of making an image in a FITS file, print out the results to an ascii
  ! file
  if(ascii) then
     open(1, file='outflows'//TRIM(nchar)//TRIM(suffix)//'.txt')

     ! write a header giving the x, y, z boundaries, as well as dt_outflow
     write(1,*) xxmin*boxlen, xxmax*boxlen, yymin*boxlen, yymax*boxlen, &
          zzmin*boxlen, zzmax*boxlen, &
          dt_outflow_in

     !OR
     if (.not. shells) then
        ! for each surface, write its z-position, upward and downward mass flux
        do ind_surf=1, n_surf
           ! convert masses to Msun/yr
           write(1,"(F10.5,ES15.4, ES15.4)") z_surf_in(ind_surf), &
                mass_flux_up(ind_surf)*scale_m_Msun / (dt_outflow_in*1d6), &
                mass_flux_down(ind_surf)*scale_m_Msun / (dt_outflow_in*1d6)
        enddo

     else

        ! for each shell, write its radius, outward and inward mass flux
        do ind_shell=1, n_shell
           ! convert masses to Msun/yr
           write(1,"(F10.5,ES15.4, ES15.4)") R_shell_in(ind_shell), &
                mass_flux_out(ind_shell)*scale_m_Msun / (dt_outflow_in*1d6), &
                mass_flux_in(ind_shell)*scale_m_Msun / (dt_outflow_in*1d6)
        enddo
        !

     endif
     close(1)
     write(*,*) "Finished writing outflows"//TRIM(nchar)//TRIM(suffix)//'.txt'
     stop
  endif


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains
  subroutine read_params
    implicit none

    integer::iargc
    character(len=8)::opt
    character(len=128)::arg
    namelist /size/ dir, xmin, xmax, ymin, ymax, zmin, zmax
    namelist /amr/ ascii, pdf, psd, lmax, typ, maxval, maxrho, pdfmin, &
         & pdfmax, pdfn, threshold, &
         & Tmin, Tmax, nbins_T, maxall, &
         & z_surf_in, R_shell_in, dt_outflow_in

    n = iargc()
    if (n < 1) then
       print *, 'usage: outflows -inp input_dir [-nml namelist] [-out suffix]'&
            //' [-ascii] [-pdf] [-psd]'&
            //' [-shells]'
       print *, ''
       print *, '  Set ascii to output the cell data as a particle list'
       print *, '  Set pdf to compute the pdf'
       print *, '  All namelist parameters can be forced if given in the'&
            //'cmd line.'
       print *, ''
       print *, '   -dir     projection (default: z)'
       print *, '   -xmin    selection, also for y and z (default: 0.0)'
       print *, '   -xmax    selection, also for y and z (default: 1.0)'
       print *, '   -lmax    maximum refinement level'
       print *, '   -typ     data to be plotted (see below)'
       print *, '   -maxval  get the maximum value along LOS '&
            //'(default: no)'
       print *, '   -maxrho  get the value where rho is maximum along'&
            //'LOS (default: no)'
       print *, '   -pdfmin  min density for the pdf [H/cc] (if pdf is set)'
       print *, '   -pdfmax  max density for the pdf [H/cc] (if pdf is set)'
       print *, '   -pdfn    number of log bin for the pdf (if pdf is set)'
       print *, '   -thres   density threshold (ascii only)'
       print *, '   -rand    mass of the particle randomly distributed in '&
            //'the cell (in Msun)'
       print *, '   -maxall  find maximum cell values within the defined volume'
       print *, '   -z_surf  check for outflows through a surface at this z '&
            //'coordinate (in kpc)'
       print *, '   -R_shell check for outflows through a shell at this radius'&
            //'(in kpc)'
       print *, '   -dt_out  time over which to check for outflows through each'&
            //'surface, in Myrs'
       print *, ''
       print *, ' typ :-1 = cpu number'
       print *, '       0 = ref. level'
       print *, '       1 = gas density (default)'
       print *, '       2 = X velocity'
       print *, '       3 = Y velocity'
       print *, '       4 = Z velocity'
       print *, '       5 = gas pressure'
       print *, '       6 = gas metallicity'
       print *, '       7 = gas temperature'
       print *, '       8 = sound speed'
       print *, '       9 = Jeans length'
       print *, '      10 = free-fall time'
       stop
    end if

    i = 1
    do while(i.le.n)
       call getarg(i,opt)
       select case (opt)
       case ('-inp')
          call getarg(i+1,arg)
          repository = trim(arg)        
       case ('-nml')
          call getarg(i+1,arg)
          open(1,file=trim(arg))
          read(1,size)
          read(1,amr)
          close(1)
       case ('-out')
          call getarg(i+1,arg)
          suffix = trim(arg)
          if(len(TRIM(suffix)).ne.0)suffix = '_'//TRIM(suffix)
       case ('-ascii')
          ascii = .true.
          i = i-1
       case ('-pdf')
          pdf = .true.
          i = i-1
       case ('-psd')
          psd = .true.
          i = i-1
       case ('-maxval')
          maxval = .true.
          i = i-1
       case ('-maxrho')
          maxrho = .true.
          i = i-1

       case ('-dir')
          call getarg(i+1,arg)
          dir = trim(arg) 
       case ('-xmin')
          call getarg(i+1,arg)
          read (arg,*) xmin
       case ('-xmax')
          call getarg(i+1,arg)
          read (arg,*) xmax
       case ('-ymin')
          call getarg(i+1,arg)
          read (arg,*) ymin
       case ('-ymax')
          call getarg(i+1,arg)
          read (arg,*) ymax
       case ('-zmin')
          call getarg(i+1,arg)
          read (arg,*) zmin
       case ('-zmax')
          call getarg(i+1,arg)
          read (arg,*) zmax
       case ('-lmax')
          call getarg(i+1,arg)
          read (arg,*) lmax
       case ('-typ')
          call getarg(i+1,arg)
          read (arg,*) typ
       case ('-pdfmin')
          call getarg(i+1,arg)
          read (arg,*) pdfmin
       case ('-pdfmax')
          call getarg(i+1,arg)
          read (arg,*) pdfmax
       case ('-pdfn')
          call getarg(i+1,arg)
          read (arg,*) pdfn
       case ('-thres')
          call getarg(i+1,arg)
          read (arg,*) threshold
       case ('-rand')
          call getarg(i+1,arg)
          read (arg,*) partmass

          ! rho-T diagram variables
       case ('-rhoT')
          rho_T = .true.
          i = i-1
       case ('-nbins_T')
          call getarg(i+1, arg)
          read (arg,*) nbins_T
       case ('-Tmin')
          call getarg(i+1, arg)
          read (arg,*) Tmin
       case ('-Tmax')
          call getarg(i+1, arg)
          read (arg,*) Tmax

       case('-maxall')
          maxall = .true.
          i = i - 1

          ! outflows variables
       case('-dt_out')
          call getarg(i+1, arg)
          read (arg, *) dt_outflow_in
       case('-z_surf')
          call getarg(i+1, arg)
          read(arg, *) z_surf_in(1)
          !!          z_surf(2) = -1.0 * z_surf(1)
          !OR
       case('-R_shell')
          call getarg(i+1, arg)
          read(arg, *) R_shell_in(1)
          !

       case default
          print '("unknown option ",a8," ignored")', opt
          i = i-1
       end select
       i = i+2
    end do
    return

  end subroutine read_params

  subroutine Box_Sphere_Intersect(ret, Bmin, Bmax, C, r, mode )
    implicit none
    integer,parameter::n=3                      ! The dimension of the space.
    real(kind=8),intent(in),dimension(n)::Bmin  ! Minimum of the box for each axis.
    real(kind=8),intent(in),dimension(n)::Bmax  ! Maximum of the box for each axis.
    real(kind=8),intent(in),dimension(n)::C     ! The sphere center in n-space.
    real(kind=8),intent(in)::r                  ! The radius of the sphere.
    integer::mode                               ! Selects hollow or solid.

    real(kind=8)::a, b
    real(kind=8)::dmin, dmax
    real(kind=8)::r2
    integer(kind=4)::i
    logical::face
    logical,intent(out)::ret

    r2 = r**2
    ret=.false.

    select case( mode )
    case (0) ! Hollow Box - Hollow Sphere
       dmin = 0
       dmax = 0
       face = .FALSE.
       do i = 1, n
          a = (C(i) - Bmin(i))**2
          b = (C(i) - Bmax(i))**2
          dmax = dmax + MAX(a,b)
          if(C(i) .lt. Bmin(i)) then
             face = .TRUE.
             dmin = dmin + a
          else if(C(i) .gt. Bmax(i)) then
             face = .TRUE.
             dmin = dmin + b
          else if(MIN(a,b) .le. r2) then
             face = .TRUE.
          endif
       enddo
       if(face .and. (dmin .le. r2) .and. (r2 .le. dmax)) then
          ret = .TRUE.
       endif

    case (1) ! Hollow Box - Solid Sphere
       dmin = 0
       face = .FALSE.
       do i = 1, n
          if(C(i) .lt. Bmin(i)) then
             face = .TRUE.
             dmin = dmin + (C(i) - Bmin(i))**2
          else if(C(i) .gt. Bmax(i)) then
             face = .TRUE.
             dmin = dmin + (C(i) - Bmax(i))**2
          else if(C(i) - Bmin(i) .le. r) then
             face = .TRUE.
          else if(Bmax(i) - C(i) .le. r) then
             face = .TRUE.
          endif
       enddo
       if(face .and. (dmin .le. r2)) then
          ret = .TRUE.
       endif

    case (2) ! Solid Box - Hollow Sphere
       dmax = 0
       dmin = 0
       do i = 1, n
          a = (C(i) - Bmin(i))**2
          b = (C(i) - Bmax(i))**2
          dmax = dmax + MAX(a,b)
          if(C(i) .lt. Bmin(i)) then
             dmin = dmin + a
          else if(C(i) .gt. Bmax(i)) then
             dmin = dmin + b
          endif
       enddo
       if(dmin .le. r2 .and. r2 .le. dmax ) then
          ret = .TRUE.
       endif

    case (3) ! Solid Box - Solid Sphere
       dmin = 0;
       do i = 1, n
          if(C(i) .lt. Bmin(i)) then
             dmin = dmin + (C(i) - Bmin(i))**2
          else if(C(i) .gt. Bmax(i)) then
             dmin = dmin + (C(i) - Bmax(i))**2
          endif
       enddo
       if(dmin .le. r2) then
          ret = .TRUE.
       endif
    end select

  end subroutine Box_Sphere_Intersect

end program outflows


!=======================================================================
!=======================================================================
!=======================================================================

subroutine deletefile(filename,status) !  Delete a FITS file

  integer::status,unit,blocksize
  character(*)::filename

  if (status .gt. 0) return

  call ftgiou(unit,status) ! Get an unused Logical Unit Number
  call ftopen(unit,filename,1,blocksize,status) ! Try to open the file

  if (status .eq. 0)then ! file is opened: delete it
     call ftdelt(unit,status)
  else if (status .eq. 103)then ! file doesn't exist:
     status=0                    ! reset status and clear errors
     call ftcmsg
  else ! there was some other error opening the file: delete the file anyway
     status=0
     call ftcmsg
     call ftdelt(unit,status)
  end if

  call ftfiou(unit, status) ! Free the unit number

end subroutine deletefile

!==================================================
!==================================================
!==================================================

subroutine hilbert3d(x,y,z,order,bit_length,npoint)
  implicit none

  integer,intent(in)::bit_length, npoint
  integer,intent(in),dimension(1:npoint)::x, y, z
  !JMG -- the following causes rank mismatch compile errors with some compilers
  real(kind=8),intent(out),dimension(1:npoint)::order
  !  real(kind=8), intent(out) :: order


  logical,dimension(0:3*bit_length-1)::i_bit_mask
  logical,dimension(0:1*bit_length-1)::x_bit_mask, y_bit_mask, z_bit_mask
  integer,dimension(0:7,0:1,0:11)::state_diagram
  integer::i, ip, cstate, nstate, b0, b1, b2, sdigit, hdigit

  if(bit_length>bit_size(bit_length))then
     write(*,*)'Maximum bit length=',bit_size(bit_length)
     write(*,*)'stop in hilbert3d'
     stop
  endif

  state_diagram = RESHAPE( (/   1, 2, 3, 2, 4, 5, 3, 5,&
       &   0, 1, 3, 2, 7, 6, 4, 5,&
       &   2, 6, 0, 7, 8, 8, 0, 7,&
       &   0, 7, 1, 6, 3, 4, 2, 5,&
       &   0, 9,10, 9, 1, 1,11,11,&
       &   0, 3, 7, 4, 1, 2, 6, 5,&
       &   6, 0, 6,11, 9, 0, 9, 8,&
       &   2, 3, 1, 0, 5, 4, 6, 7,&
       &  11,11, 0, 7, 5, 9, 0, 7,&
       &   4, 3, 5, 2, 7, 0, 6, 1,&
       &   4, 4, 8, 8, 0, 6,10, 6,&
       &   6, 5, 1, 2, 7, 4, 0, 3,&
       &   5, 7, 5, 3, 1, 1,11,11,&
       &   4, 7, 3, 0, 5, 6, 2, 1,&
       &   6, 1, 6,10, 9, 4, 9,10,&
       &   6, 7, 5, 4, 1, 0, 2, 3,&
       &  10, 3, 1, 1,10, 3, 5, 9,&
       &   2, 5, 3, 4, 1, 6, 0, 7,&
       &   4, 4, 8, 8, 2, 7, 2, 3,&
       &   2, 1, 5, 6, 3, 0, 4, 7,&
       &   7, 2,11, 2, 7, 5, 8, 5,&
       &   4, 5, 7, 6, 3, 2, 0, 1,&
       &  10, 3, 2, 6,10, 3, 4, 4,&
       &   6, 1, 7, 0, 5, 2, 4, 3 /), &
       & (/8 ,2, 12 /) )

  do ip=1,npoint
     ! convert to binary
     do i=0,bit_length-1
        x_bit_mask(i)=btest(x(ip),i)
        y_bit_mask(i)=btest(y(ip),i)
        z_bit_mask(i)=btest(z(ip),i)
     enddo

     ! interleave bits
     do i=0,bit_length-1
        i_bit_mask(3*i+2)=x_bit_mask(i)
        i_bit_mask(3*i+1)=y_bit_mask(i)
        i_bit_mask(3*i  )=z_bit_mask(i)
     end do

     ! build Hilbert ordering using state diagram
     cstate=0
     do i=bit_length-1,0,-1
        b2=0
        if(i_bit_mask(3*i+2))b2=1
        b1=0
        if(i_bit_mask(3*i+1))b1=1
        b0=0
        if(i_bit_mask(3*i  ))b0=1

        sdigit=b2*4+b1*2+b0
        nstate=state_diagram(sdigit,0,cstate)
        hdigit=state_diagram(sdigit,1,cstate)
        i_bit_mask(3*i+2)=btest(hdigit,2)
        i_bit_mask(3*i+1)=btest(hdigit,1)
        i_bit_mask(3*i  )=btest(hdigit,0)
        cstate=nstate
     enddo

     ! save Hilbert key as double precision real
     order(ip)=0.
     do i=0,3*bit_length-1
        b0=0 ; if(i_bit_mask(i))b0=1
        order(ip)=order(ip)+dble(b0)*dble(2)**i
     end do
  end do

end subroutine hilbert3d

!===================================================
! Combine all the physical properties of an AMR cell into
! a single array.
!===================================================
subroutine get_cell(var, position, grid_index, cell_index, &
     ngrida, nvarh, ndim, ilevel, &
     boxlen, scale_vkms, scale_nH, scale_T2, cell)
  implicit none

  integer, INTENT(in) :: ilevel, grid_index, cell_index,ngrida, nvarh, ndim
  real(kind=8), INTENT(in), dimension(1:ngrida,1:ndim) :: position
  real(kind=8), INTENT(in), dimension(1:ngrida,1:8,1:nvarh) :: var
  real(kind=8), INTENT(in) :: boxlen, scale_vkms, scale_nH, scale_T2
  real(kind=8), INTENT(out), dimension(9) :: cell

  integer :: i, k=1 , ind

  !  write(*,*) "AHH", ilevel
  !  WRITE(*,*) "22222", grid_index, cell_index, ind
  !  WRITE(*,*) "33333", var(1,1,1)
  i = grid_index
  ind = cell_index

  !  write(*,*) "no trouble yet", cell(3), cell(6)
  !  write(*,*) "|", boxlen, scale_vkms, i, k
  do k=1, 3
     cell(k) = position(i,k)*boxlen
     cell(k+3) = var(i,ind,k+1)*scale_vkms
  enddo

  cell(7) = var(i,ind,1)*scale_nH
  cell(8) = var(i,ind,5)*scale_T2/ (var(i,ind,1))

  cell(9) = dble(ilevel)

  !  write(*,*) i, ind, cell(1)
end subroutine get_cell


!=======================================================================
!=======================================================================
! Find the area of overlap between two squares, e.g.
! an AMR grid cell and an image pixel.  This assumes the 
! squares are aligned so that their faces are parallell.
!=======================================================================
subroutine area_overlap_squares(center_A, width_A, center_B, width_B,&
     overlap_area)

  implicit none

  integer, parameter :: ndim=2  !!! 2-D squares, not 3-D cubes
  real(kind=8), INTENT(in), DIMENSION(ndim) :: center_A, center_B
  real(kind=8), INTENT(in) :: width_A, width_B
  real(kind=8), INTENT(out) :: overlap_area

  integer :: i
  real(kind=8), dimension(ndim) :: overlap_length
  real(kind=8) :: left_A, left_B, right_A, right_B

  ! loop over dimensions, treating each independently
  do i=1, ndim
     ! define the boundaries of the squares in this dimension
     left_A = center_A(i) - width_A / 2.0
     right_A = center_A(i) + width_A / 2.0

     left_B = center_B(i) - width_B / 2.0
     right_B = center_B(i) + width_B / 2.0

     ! 6 possible cases: A<B, A>B (no overlap)
     !   A is within B, or B is within A
     !   A partly overlaps B on the right, or the left
     ! no overlap
     if(right_A <= left_B .or. right_B <= left_A) then
        overlap_length(i) = 0.0
     else
        ! B is within A
        if(left_A <= left_B .and. right_A >= right_B) then
           overlap_length(i) = width_B
        else
           ! A is within B
           if(left_B <= left_A .and. right_B >= right_A) then 
              overlap_length(i) = width_A
           else
              ! partial overlap with A left of B
              if(left_A <= left_B .and. right_A <= right_B .and. &
                   left_B <= right_A) then
                 overlap_length(i) = right_A - left_B
              else 
                 ! partial overlap with B left of A
                 if(left_B <= left_A .and. right_B <= right_A .and. &
                      left_A <= right_B ) then
                    overlap_length(i) = right_B - left_A
                 else
                    write(*,*) "ERROR!", left_A, right_A, left_B, right_B
                    stop
                 endif
              endif  ! partial overlap
           endif   ! A within B

        endif ! B within A
     endif   !no overlap
  enddo

  ! calculate the overlap area by multiplying the independent lengths
  overlap_area = 1.0
  do i=1, ndim
     overlap_area = overlap_area * overlap_length(i)
  enddo

end subroutine area_overlap_squares

