program post_cloudy_parameters

  !ce programme parcourt les cellules du cube AMR et trouve le point Cloudy le plus proche de celle-ci.

  implicit none

  real(kind=8)::pi=2.d0*acos(0.d0)

  real(kind=8)::var1, var2, var6, var7, var8, var9

  integer(kind=8)::number_of_leaves=0, number_of_lop_points=0, i, j, j_min, ierr
  integer(kind=8),dimension(:),allocatable::remaining_lop_points, in_cell
  real(kind=8)::boxlen, l_max

  real(kind=8),dimension(:),allocatable::x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, &
       &temperature_amr, cell_size_amr
  integer(kind=8),dimension(:),allocatable::ilevel_amr
  real(kind=8),dimension(:),allocatable::x_lop, y_lop, z_lop, theta_lop, phi_lop, &
       &depth_lop, density_lop,temperature_lop, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, &
       &OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop
  character(len=3),dimension(:),allocatable::flag
  character(len=35),dimension(:),allocatable::name


  character(len=3)::part,factor, NUMBER
  character(len=300)::file_amr,file_lop
  character(len=2)::lm
  character(len=6)::sim, fb_case, sm, nm
  logical:: up=.false., dn=.false., cu=.false., cd=.false., dk=.false.
  logical:: xz=.false., zy=.false., px=.false., py=.false., cx=.false., cy=.false.
  logical::only_once=.true.
  integer(kind=8)::n_min, n_max
  character(len=15)::nmin,nmax

  real(kind=8)::x_center,y_center,z_center


  real(kind=8)::distance, former_distance
  real(kind=8)::time1,time2, time_before, time_loop

  real(kind=8)::distance_min,x_lop_min, y_lop_min, z_lop_min, theta_lop_min, phi_lop_min, depth_lop_min, &
       &density_lop_min, temperature_lop_min, Hionfrac_lop_min, Hb_4861A_lop_min, OIII_5007A_lop_min, &
       &Ha_6563A_lop_min, NII_6584A_lop_min

  !SFR et masse
  real(kind=8),dimension(:),allocatable::SFR100_amr,SFR100_cloudy,SFR100_hybrid
  real(kind=8)::SFR100_incell_amr=0,SFR100_incell_cloudy=0,SFR100_incell_hybrid=0, count_in_cell=0
  real(kind=8)::SFR100_in4cell_amr=0,SFR100_in4cell_cloudy=0,SFR100_in4cell_hybrid=0, count_in_4cell=0
  real(kind=8),dimension(:),allocatable::SFR10_amr,SFR10_cloudy,SFR10_hybrid
  real(kind=8)::SFR10_incell_amr=0,SFR10_incell_cloudy=0,SFR10_incell_hybrid=0
  real(kind=8)::SFR10_in4cell_amr=0,SFR10_in4cell_cloudy=0,SFR10_in4cell_hybrid=0
  real(kind=8),dimension(:),allocatable::SFR1_amr,SFR1_cloudy,SFR1_hybrid
  real(kind=8)::SFR1_incell_amr=0,SFR1_incell_cloudy=0,SFR1_incell_hybrid=0
  real(kind=8)::SFR1_in4cell_amr=0,SFR1_in4cell_cloudy=0,SFR1_in4cell_hybrid=0
  real(kind=8),dimension(:),allocatable::SFR0_amr,SFR0_cloudy,SFR0_hybrid
  real(kind=8)::SFR0_incell_amr=0,SFR0_incell_cloudy=0,SFR0_incell_hybrid=0
  real(kind=8)::SFR0_in4cell_amr=0,SFR0_in4cell_cloudy=0,SFR0_in4cell_hybrid=0


  real(kind=8)::M_incell_amr=0,M_incell_cloudy=0,M_tot_amr=0,M_tot_cloudy=0, M_incell_heated_amr=0, M_incell_heated_cloudy=0
  real(kind=8)::M_in4cell_amr=0,M_in4cell_cloudy=0,M_in4cell_heated_amr=0, M_in4cell_heated_cloudy=0
  real(kind=8)::epsilon=0.01d00,n_kennicutt=1.5d00,G_grav=6.67d-11,hcc_to_kgm3=1.67d-21,kgs_to_Msunyr=1.58d-23,kpc_to_m=1.d0/3.24d-20
  real(kind=8)::cst_SFR,scale_SFR, scale_mass, scale_SFR_max, SFR_amr_max

  real(kind=8)::count_sfr100_hybrid=0,count_sfr100_cloudy=0,count_sfr100_amr=0
  real(kind=8)::count_incell_sfr100_hybrid=0,count_incell_sfr100_cloudy=0,count_incell_sfr100_amr=0
  real(kind=8)::count_in4cell_sfr100_hybrid=0,count_in4cell_sfr100_cloudy=0,count_in4cell_sfr100_amr=0
  real(kind=8)::count_sfr10_hybrid=0,count_sfr10_cloudy=0,count_sfr10_amr=0
  real(kind=8)::count_incell_sfr10_hybrid=0,count_incell_sfr10_cloudy=0,count_incell_sfr10_amr=0
  real(kind=8)::count_in4cell_sfr10_hybrid=0,count_in4cell_sfr10_cloudy=0,count_in4cell_sfr10_amr=0
  real(kind=8)::count_sfr1_hybrid=0,count_sfr1_cloudy=0,count_sfr1_amr=0
  real(kind=8)::count_incell_sfr1_hybrid=0,count_incell_sfr1_cloudy=0,count_incell_sfr1_amr=0
  real(kind=8)::count_in4cell_sfr1_hybrid=0,count_in4cell_sfr1_cloudy=0,count_in4cell_sfr1_amr=0
  real(kind=8)::count_sfr0_hybrid=0,count_sfr0_cloudy=0,count_sfr0_amr=0
  real(kind=8)::count_incell_sfr0_hybrid=0,count_incell_sfr0_cloudy=0,count_incell_sfr0_amr=0
  real(kind=8)::count_in4cell_sfr0_hybrid=0,count_in4cell_sfr0_cloudy=0,count_in4cell_sfr0_amr=0
  integer(kind=8)::N_incells=4 !pourra etre calcule pour chaque cellule selon la longueur de Jeans (au moins 4, voire 20)
  real(kind=8)::temperature_post
  real(kind=8)::nb_heated_cells=0,nb_heated_incell_cells=0,nb_heated_in4cell_cells=0

  integer(kind=8)::len_host,status_host
  character(len=15)::host
  logical::curie=.false.,mac=.false.,irfucoast=.false., curie2=.false.

  real(kind=8)::T_polytrope,alpha_jeans,density_0

  call GET_ENVIRONMENT_VARIABLE("HOSTNAME", host)
  print*, trim(host)

  if (trim(host) == 'curie51') then
     curie = .true.
  else if (trim(host) == 'sapmcr111' .or. trim(host) == '') then
     mac = .true.
  else if (trim(host) == 'irfucoast') then
     irfucoast = .true.
  else if (trim(host) == 'curie71') then
     curie2 = .true.
     !else
     !    print*, 'Attention, machine non reconnue !'
     !
     !stop
  endif

  !curie2 = .true.

  call CPU_TIME(time1)

  call getarg(1,part)
  call getarg(2,sim)
  call getarg(3,factor)
  call getarg(4,nmin)
  call getarg(5,nmax)
  call getarg(6,lm)

  if (trim(lm) == '') lm = '13'

  print*, part, sim, factor, nmin, nmax, lm

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (trim(lm) == '12') then
     host = 'cloudy_lores'
  endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  read(nmin,'(I15)') n_min
  read(nmax,'(I15)') n_max

  part=trim(part)
  factor=trim(factor)
  if (factor /= '0') then
     nm = 'x'//trim(factor)//'_'
  else
     nm = ''
  endif

  !if (part == 'all') then
  !up = .true.; dn = .true.; cu = .true.; cd = .true.; dk = .true.
  !xz = .true.; zy = .true.; px = .true.; py = .true.; cx = .true.; cy = .true.
  !else if (part == 'up') then
  !up = .true.
  !else if (part == 'dn') then
  !dn = .true.
  !else if (part == 'dk') then
  !dk=.true.
  !else if (part == 'cu') then
  !cu=.true.
  !else if (part == 'cd') then
  !cd=.true.
  !else if (part == 'xz') then
  !xz=.true.
  !else if (part == 'zy') then
  !zy=.true.
  !else if (part == 'px') then
  !px=.true.
  !else if (part == 'py') then
  !py=.true.
  !else if (part == 'cx') then
  !cx=.true.
  !else if (part == 'cy') then
  !cy=.true.
  !else
  if (part == '') then
     print*, "Usage : part, sim, factor, n_min, n_max"
     print*, "part : all/xz/zy/up/dn/..."
     print*, "sim : 00100/00085/00075w/00075n"
     print*, "factor : 0 (default)/10/100"
     print*, "n_min/n_max : intervalle de cellules AMR a traiter (de 1 a number_of_leaves)."
     print*, "lmax"
     stop
  endif

  sm=sim

  if (sim == '00085' .or. sim == '00075n' &
       &.or. sim == '00090' .or. sim == '00120' .or. sim == '00130' .or. sim == '00148') then
     fb_case = 'No'
     if (sim == '00075n') then
        sm = '00075'
     endif
  endif

  if (sim == '00100' .or. sim == '00075w' &
       &.or. sim == '00108' .or. sim == '00150' .or. sim == '00170' .or. sim == '00210') then
     fb_case = 'With'
     if (sim == '00075w') then
        sm = '00075'
     endif
  endif


  if (mac) then
     open(9, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (curie) then
     open(9, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (irfucoast) then
     open(9, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (curie2) then
     open(9, file="/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  else if (host == 'cloudy_lores') then
     print*, sm
     print*, "/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/output_"//trim(sm)//"/sink_"//trim(sm)//".out"
     open(9, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/output_"//trim(sm)//"/sink_"//trim(sm)//".out", &
          & form="formatted")
  endif

  do j = 1,4
     read(9,*)
  enddo
  read(9,*) var1, var2, x_center, y_center, z_center, var6, var7, var8, var9
  write(*,*) "Position of the AGN : ",  x_center, y_center, z_center
  close(9)

  if (mac) then
     open(10, file="/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (curie) then
     open(10, file="/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (irfucoast) then
     open(10, file="/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (curie2) then
     open(10, file="/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/statistics_"//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  else if (trim(host) == 'cloudy_lores') then
     open(10, file="/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/statistics_"&
          &//trim(sim)//".ascii.lmax"//trim(lm), &
          & form="formatted")
  endif

  read(10,*) number_of_leaves, boxlen, l_max
  write(*,*) "n_leaves, boxlen (kpc), lmax ", number_of_leaves, boxlen, l_max
  !number           !kpc     !number

  !write(*,*) fb_case, sim, lm
  !write(*,*) "/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
  !&"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)

  !fichier contenant la liste des cellules de la boite AMR
  if (mac) then
     file_amr = "/Users/oroos/Post-stage/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (curie) then
     file_amr = "/ccc/work/cont005/gen2192/juneaus/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (irfucoast) then
     file_amr = "/gpfs/data2/oroos/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (curie2) then
     file_amr = "/ccc/work/cont003/dsm/rooso/orianne_data/"//trim(fb_case)//&
          &"_AGN_feedback/gas_part_"//trim(sim)//".ascii.lmax"//trim(lm)
  else if (trim(host) == 'cloudy_lores') then
     file_amr = "/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/output/gas_part_"&
          &//trim(sim)//".ascii.lmax"//trim(lm)

  endif


  open(11, file=trim(file_amr), form="formatted",iostat=ierr,status="old")

  if (ierr/=0) then
     write(*,*) "Impossible d'ouvrir le fichier "//trim(file_amr)

     stop
  endif

  allocate(x_amr(1:number_of_leaves))
  allocate(y_amr(1:number_of_leaves))
  allocate(z_amr(1:number_of_leaves))
  allocate(vx_amr(1:number_of_leaves))
  allocate(vy_amr(1:number_of_leaves))
  allocate(vz_amr(1:number_of_leaves))
  allocate(density_amr(1:number_of_leaves))
  allocate(temperature_amr(1:number_of_leaves))
  allocate(cell_size_amr(1:number_of_leaves))
  allocate(ilevel_amr(1:number_of_leaves))
  allocate(SFR100_amr(1:number_of_leaves))
  allocate(SFR100_cloudy(1:number_of_leaves))
  allocate(SFR100_hybrid(1:number_of_leaves))
  allocate(SFR10_amr(1:number_of_leaves))
  allocate(SFR10_cloudy(1:number_of_leaves))
  allocate(SFR10_hybrid(1:number_of_leaves))
  allocate(SFR1_amr(1:number_of_leaves))
  allocate(SFR1_cloudy(1:number_of_leaves))
  allocate(SFR1_hybrid(1:number_of_leaves))
  allocate(SFR0_amr(1:number_of_leaves))
  allocate(SFR0_cloudy(1:number_of_leaves))
  allocate(SFR0_hybrid(1:number_of_leaves))
  allocate(in_cell(1:number_of_leaves))


  !fichier contenant la liste des LOPs passees dans cloudy correspondantes
  if (mac) then
     file_lop = '/Users/oroos/Post-stage/LOPs'//trim(sim)//'/post_cloudy_parameters_'&
          &//trim(nm)//trim(sim)//'_'//trim(part)//'.dat'
  else if (curie) then
     file_lop = '/ccc/work/cont005/gen2192/juneaus/post_cloudy_parameters_'&
          &//trim(nm)//trim(sim)//'_'//trim(part)//'.dat'
  else if (irfucoast) then
     file_lop = '/gpfs/data2/oroos/post_cloudy_parameters_'&
          &//trim(nm)//trim(sim)//'_'//trim(part)//'.dat'
  else if (curie2) then
     file_lop = '/ccc/work/cont003/dsm/rooso/Tests_c13.02d0/post_cloudy_parameters_'&
          &//trim(nm)//trim(sim)//'_'//trim(part)//'.dat'
  else if (trim(host) == 'cloudy_lores') then
     file_lop = '/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/lores_post_cloudy_parameters_'&
          &//trim(nm)//trim(sim)//'_'//trim(part)//'.dat'
  endif

  open(12, file=trim(file_lop), form="formatted",iostat=ierr,status="old")

  if (ierr/=0) then
     write(*,*) "Impossible d'ouvrir le fichier "//trim(file_lop)
     stop
  endif


  !on lit (et compte le nombre de lignes  correctement lues)
  !   tant qu'il n'y a pas d'erreur de lecture.
  do while (ierr==0)
     read(12,*,iostat=ierr) number
     if (ierr==0) then ! la lecture s'est bien passee
        ! on incremente le compteur du nombre de lignes
        number_of_lop_points = number_of_lop_points+1
        !print*, number_of_lop_points
     endif
  enddo


  write(*,*) 'le fichier '//trim(file_lop)//' contient ',number_of_lop_points,'lignes'

  allocate(x_lop(1:number_of_lop_points))
  allocate(y_lop(1:number_of_lop_points))
  allocate(z_lop(1:number_of_lop_points))
  allocate(theta_lop(1:number_of_lop_points))
  allocate(phi_lop(1:number_of_lop_points))
  allocate(depth_lop(1:number_of_lop_points))
  allocate(density_lop(1:number_of_lop_points))
  allocate(temperature_lop(1:number_of_lop_points))
  allocate(Hneutre_lop(1:number_of_lop_points))
  allocate(Oneutre_lop(1:number_of_lop_points))
  allocate(Hb_4861A_lop(1:number_of_lop_points))
  allocate(OIII_5007A_lop(1:number_of_lop_points))
  allocate(Ha_6563A_lop(1:number_of_lop_points))
  allocate(NII_6584A_lop(1:number_of_lop_points))
  allocate(remaining_lop_points(1:number_of_lop_points))
  allocate(flag(1:number_of_lop_points))
  allocate(name(1:number_of_lop_points))


  if (mac) then
     open(14,file='/Users/oroos/Post-stage/LOPs'//trim(sim)//'/'//trim(nm)//'cloudy_near_amr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (curie) then
     open(14,file='/ccc/work/cont005/gen2192/juneaus/'//trim(nm)//'cloudy_near_amr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (irfucoast) then
     open(14,file='/gpfs/data2/oroos/'//trim(nm)//'cloudy_near_amr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (curie2) then
     open(14,file='/ccc/work/cont003/dsm/rooso/Tests_c13.02d0/'//trim(nm)//'cloudy_near_amr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (trim(host) == 'cloudy_lores') then
     open(14,file='/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/'//trim(nm)//'lores_cloudy_near_amr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  endif

  rewind 12

  scale_mass = hcc_to_kgm3*kpc_to_m**3/2.0d30 !n(H/cc) and V(kpc^3) -> M(M_sun)
  scale_SFR_max = 1.67d-27/2.0d30*(1.d0/3.24d-22)**3*1d-9 ! "           "       -> SFR(M_sun/yr)

  !constantes pour le SFR :
  cst_SFR = sqrt(32.d0*G_grav/(3.d0*pi))
  scale_SFR = cst_SFR*hcc_to_kgm3**n_kennicutt*kpc_to_m**3*kgs_to_Msunyr

  !print*, epsilon*cst_SFR*hcc_to_kgm3**n_kennicutt*kpc_to_m**3*kgs_to_Msunyr


  call CPU_TIME(time_before)


  !n_min = 1
  !n_max = int(number_of_leaves/512.d0)

  print*, 'Ce programme traite les cellules AMR de ', n_min, 'a', n_max, 'sur ', number_of_leaves,'.'

  !correction du polytrope de jeans (deja faite pour les donnees cloudy)
  alpha_jeans = (0.041666d0*sqrt(32*pi)*(boxlen*1d3/2**l_max)**2)
  density_0 = 900.d0/alpha_jeans !density(T = 900 K) H/cc
  !print*, 'density_0 : ', density_0

  if (n_min > 1) then
     do j = 1, n_min-1
        read(11,*)  !passer les premieres lignes si necessaire
     enddo
  endif

  do i = n_min, n_max

     read(11,'(9d20.12,1I5)') x_amr(i), y_amr(i), z_amr(i), vx_amr(i), vy_amr(i), vz_amr(i), &
          &density_amr(i), temperature_amr(i), cell_size_amr(i), ilevel_amr(i)
     !write(*,*) x(1), y(1), z(1), vx(1), vy(1), vz(1), rho(1), temp(1), cell_size(1), ilevel(1)
     !kpc              !km/s                !H/cc   !K       !kpc          !number
     !x, y, z, vx(mass-weighted), vy(mass-weighted), vz(mass-weighted), rho, temp, cell_size, level

     if (density_amr(i) == 0 .or. temperature_amr(i)  == 0 .or. cell_size_amr(i) == 0 .or.  ilevel_amr(i) == 0) then
        print*, 'AMR : Attention, valeurs lues nulles. ', i
        print*, density_amr(i), temperature_amr(i), cell_size_amr(i), ilevel_amr(i)
        stop
     endif

     call CPU_TIME(time_loop)
     if(time_loop - time_before > 25.d0) then
        write(*,*) real(i-n_min)/real(n_max-n_min)*100, '% proceeded...'
        time_before = time_loop
     endif

     !correction du polytrope de jeans !nb : deja fait pour les donnees des lop
     if (density_amr(i) >= density_0) then
        T_polytrope = alpha_jeans*density_amr(i)
        if (temperature_amr(i) <= 2.d0*T_polytrope) then
           temperature_amr(i) = 900 !K
        endif
     endif

     if (i==n_min) then !a ne faire qu'une fois
        print*, 'Lecture du cube de LOP.'
        do j = 1, number_of_lop_points
           read(12,*) name(j), x_lop(j), y_lop(j), z_lop(j), theta_lop(j), phi_lop(j), depth_lop(j), &
                & density_lop(j), temperature_lop(j), &
                & Hneutre_lop(j), Oneutre_lop(j), Hb_4861A_lop(j), OIII_5007A_lop(j), &
                & Ha_6563A_lop(j), NII_6584A_lop(j), remaining_lop_points(j), flag(j)
           !if (j<=5) write(*,*) x_lop(j), j, remaining_lop_points(j)

           !print*, part_lop(j), x_lop(j)


           !NB : il faut prendre le max de T_lop_jmin et T_AMR !!!

           if (x_lop(j) == 0 .and. y_lop(j) == 0 .and. z_lop(j) == 0 &
                & .and. theta_lop(j) == 0 .and. phi_lop(j) == 0) then
              print*, 'LOP : Attention, valeurs lues nulles. ', j, i
              print*, x_lop(j), y_lop(j), z_lop(j), theta_lop(j), phi_lop(j)
              stop
           endif
        enddo
        print*, 'Fin de la lecture du cube de LOP.'
        print*, 'min/max de density_lop : ', minval(density_lop),'/',maxval(density_lop)
     endif

     distance_min = 1d28
     former_distance = 1d28

     j = 1
     j_min = -1
     do while (j <= number_of_lop_points)

        !optimisation :
        !ne calculer les carres que si necessaire


        if (j==1 .or. abs(x_lop(j) - x_amr(i)) < former_distance) then
           if (j==1 .or. abs(y_lop(j) - y_amr(i)) < former_distance) then
              if (j==1 .or. abs(z_lop(j) - z_amr(i)) < former_distance) then


                 distance = sqrt((x_amr(i) - x_lop(j))**2 + (y_amr(i)- y_lop(j))**2 + (z_amr(i) - z_lop(j))**2)

                 if (distance < distance_min) then
                    distance_min = distance
                    j_min = j
                    !print*, distance_min, i
                 endif
                 !if (distance > distance_min .and. distance > former_distance) then
                 !    j = j + remaining_lop_points(j)
                 !endif

                 former_distance = distance

              endif
           endif
        endif
        if (remaining_lop_points(j) == 0) former_distance = 1d28

        j = j + 1
     end do

     !print*, x_amr(i), y_amr(i), z_amr(i), cell_size_amr(i), i
     !print*, x_lop(j_min), y_lop(j_min), z_lop(j_min), distance_min

     !if (distance_min <= cell_size_amr(i)) print*, distance_min, cell_size_amr(i), i

     if (( x_LOP(j_min) >= (x_amr(i) - N_incells*cell_size_amr(i)/2.d0) ) &
          & .and. ( x_LOP(j_min) < (x_amr(i) + N_incells*cell_size_amr(i)/2.d0 ) ) .and. &
          &   ( y_LOP(j_min) >= (y_amr(i) - N_incells*cell_size_amr(i)/2.d0) ) &
          & .and. ( y_LOP(j_min) < (y_amr(i) + N_incells*cell_size_amr(i)/2.d0 ) ) .and. &
          &   ( z_LOP(j_min) >= (z_amr(i) - N_incells*cell_size_amr(i)/2.d0) ) &
          & .and. ( z_LOP(j_min) < (z_amr(i) + N_incells*cell_size_amr(i)/2.d0 ) )) then

        in_cell(i) = N_incells !le point de la LOP le plus proche de la cellule est dans un cube de N_cells fois la taille de la cellule

        if   (( x_LOP(j_min) >= (x_amr(i) - cell_size_amr(i)/2.d0) ) .and. &
             & ( x_LOP(j_min) < (x_amr(i) + cell_size_amr(i)/2.d0 ) ) .and. &
             & ( y_LOP(j_min) >= (y_amr(i) - cell_size_amr(i)/2.d0) ) .and. &
             & ( y_LOP(j_min) < (y_amr(i) + cell_size_amr(i)/2.d0 ) ) .and. &
             & ( z_LOP(j_min) >= (z_amr(i) - cell_size_amr(i)/2.d0) ) .and. &
             & ( z_LOP(j_min) < (z_amr(i) + cell_size_amr(i)/2.d0 ) )) then

           in_cell(i) = 1 !le point de LOP le plus proche de la cellule est dans celle-ci
        endif
     else
        in_cell(i) = 0 !le point de la LOP le plus proche de la cellule n'est pas dans cette cellule, ni dans N_cells cellules
     endif
     !if (in_cell(i) > 0) print*, in_cell(i)


     if (i == n_min) then
        print*, 'min/max de x_lop ', minval(x_lop), '/', maxval(x_lop)
        print*, 'min/max de y_lop ', minval(y_lop), '/', maxval(y_lop)
        print*, 'min/max de z_lop ', minval(z_lop), '/', maxval(z_lop)
     endif

     !calcul du SFR correspondant :
     SFR100_amr(i) = 0
     SFR100_cloudy(i) = 0
     SFR100_hybrid(i) = 0
     SFR10_amr(i) = 0
     SFR10_cloudy(i) = 0
     SFR10_hybrid(i) = 0
     SFR1_amr(i) = 0
     SFR1_cloudy(i) = 0
     SFR1_hybrid(i) = 0
     SFR0_amr(i) = 0
     SFR0_cloudy(i) = 0
     SFR0_hybrid(i) = 0

     temperature_post = max(temperature_amr(i),temperature_lop(j_min))

     if (temperature_lop(j_min) > temperature_amr(i)) then
        !print*, 'Cellule chauffee !'
        nb_heated_cells = nb_heated_cells + 1
        if (in_cell(i) == 1) nb_heated_incell_cells = nb_heated_incell_cells + 1
        if (in_cell(i) >= 1) nb_heated_in4cell_cells = nb_heated_in4cell_cells + 1
     endif



     if (density_lop(j_min) == 0) then
        print*, 'Densite nulle !!'
        stop
     endif

     !AJOUTER DEUX AUTRES CRITERES DE SFR : 1, 10, 100 H/cc
!!!if (density_amr(i) >= 10 .and. temperature_amr(i) <= 1d4) then
!!!SFR_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
!!!SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
!!!!print*, SFR_amr(i), SFR_amr_max
!!!if (SFR_amr(i) > SFR_amr_max) then
!!!SFR_amr(i) = SFR_amr_max
     !print*, 'Reduction'
!!!endif

     if (temperature_amr(i) <= 1d4) then

        SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
        SFR0_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
        !print*, SFR0_amr(i), SFR_amr_max
        if (SFR0_amr(i) > SFR_amr_max) then
           SFR0_amr(i) = SFR_amr_max
           !print*, 'Reduction'
        endif
        if (in_cell(i) == 1) count_incell_sfr0_amr = count_incell_sfr0_amr + 1
        if (in_cell(i) >= 1) count_in4cell_sfr0_amr = count_in4cell_sfr0_amr + 1
        count_sfr0_amr = count_sfr0_amr + 1

        if (density_amr(i) >= 1) then
           SFR1_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
           !print*, SFR1_amr(i), SFR_amr_max
           if (SFR1_amr(i) > SFR_amr_max) then
              SFR1_amr(i) = SFR_amr_max
              !print*, 'Reduction'
           endif
           if (in_cell(i) == 1) count_incell_sfr1_amr = count_incell_sfr1_amr + 1
           if (in_cell(i) >= 1) count_in4cell_sfr1_amr = count_in4cell_sfr1_amr + 1
           count_sfr1_amr = count_sfr1_amr + 1


           if (density_amr(i) >= 10) then
              SFR10_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
              !print*, SFR10_amr(i), SFR_amr_max
              if (SFR10_amr(i) > SFR_amr_max) then
                 SFR10_amr(i) = SFR_amr_max
                 !print*, 'Reduction'
              endif
              if (in_cell(i) == 1) count_incell_sfr10_amr = count_incell_sfr10_amr + 1
              if (in_cell(i) >= 1) count_in4cell_sfr10_amr = count_in4cell_sfr10_amr + 1
              count_sfr10_amr = count_sfr10_amr + 1

              if (density_amr(i) >= 100) then
                 SFR100_amr(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
                 !print*, SFR100_amr(i), SFR_amr_max
                 if (SFR100_amr(i) > SFR_amr_max) then
                    SFR100_amr(i) = SFR_amr_max
                    !print*, 'Reduction'
                 endif
                 if (in_cell(i) == 1) count_incell_sfr100_amr = count_incell_sfr100_amr + 1
                 if (in_cell(i) >= 1) count_in4cell_sfr100_amr = count_in4cell_sfr100_amr + 1
                 count_sfr100_amr = count_sfr100_amr + 1

              endif
           endif
        endif
     endif

     !SFR_max (M_sun/yr) = epsilon_max*9.9d0*density_amr(10^9 M_sun/kpc^3)*V_cell(kpc^3) (from Jared, used to compute SFR_max in Ramses)
     !avec 10^9M_sunkpc3_to_Hcc = 10^9*2d30/1.67d-27*(3.24d-22)^3 = 407.33d0 (et on utilise l'inverse)


!!!if (density_lop(j_min) >= 10 .and. temperature_post <= 1d4) then
!!!   SFR_cloudy(i) = epsilon*scale_SFR*density_lop(j_min)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
!!!SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_lop(j_min)*cell_size_amr(i)**3
!!!!print*, SFR_amr(i), SFR_amr_max
!!!if (SFR_cloudy(i) > SFR_amr_max) then
!!!SFR_cloudy(i) = SFR_amr_max
!!!!print*, 'Reduction'
!!!endif
!!!    if (in_cell(i) == 1) count_incell_sfr_cloudy = count_incell_sfr_cloudy + 1
!!!    if (in_cell(i) >= 1) count_in4cell_sfr_cloudy = count_in4cell_sfr_cloudy + 1
!!!    count_sfr_cloudy = count_sfr_cloudy + 1
!!!endif

     if (temperature_post <= 1d4) then

        SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_lop(j_min)*cell_size_amr(i)**3
        SFR0_cloudy(i) = epsilon*scale_SFR*density_lop(j_min)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
        !print*, SFR0_amr(i), SFR_amr_max
        if (SFR0_cloudy(i) > SFR_amr_max) then
           SFR0_cloudy(i) = SFR_amr_max
           !print*, 'Reduction'
        endif
        if (in_cell(i) == 1) count_incell_sfr0_cloudy = count_incell_sfr0_cloudy + 1
        if (in_cell(i) >= 1) count_in4cell_sfr0_cloudy = count_in4cell_sfr0_cloudy + 1
        count_sfr0_cloudy = count_sfr0_cloudy + 1


        if (density_lop(j_min) >= 1) then
           SFR1_cloudy(i) = epsilon*scale_SFR*density_lop(j_min)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
           !print*, SFR1_amr(i), SFR_amr_max
           if (SFR1_cloudy(i) > SFR_amr_max) then
              SFR1_cloudy(i) = SFR_amr_max
              !print*, 'Reduction'
           endif
           if (in_cell(i) == 1) count_incell_sfr1_cloudy = count_incell_sfr1_cloudy + 1
           if (in_cell(i) >= 1) count_in4cell_sfr1_cloudy = count_in4cell_sfr1_cloudy + 1
           count_sfr1_cloudy = count_sfr1_cloudy + 1


           if (density_lop(j_min) >= 10) then
              SFR10_cloudy(i) = epsilon*scale_SFR*density_lop(j_min)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
              !print*, SFR10_amr(i), SFR_amr_max
              if (SFR10_cloudy(i) > SFR_amr_max) then
                 SFR10_cloudy(i) = SFR_amr_max
                 !print*, 'Reduction'
              endif
              if (in_cell(i) == 1) count_incell_sfr10_cloudy = count_incell_sfr10_cloudy + 1
              if (in_cell(i) >= 1) count_in4cell_sfr10_cloudy = count_in4cell_sfr10_cloudy + 1
              count_sfr10_cloudy = count_sfr10_cloudy + 1

              if (density_lop(j_min) >= 100) then
                 SFR100_cloudy(i) = epsilon*scale_SFR*density_lop(j_min)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
                 !print*, SFR100_amr(i), SFR_amr_max
                 if (SFR100_cloudy(i) > SFR_amr_max) then
                    SFR100_cloudy(i) = SFR_amr_max
                    !print*, 'Reduction'
                 endif
                 if (in_cell(i) == 1) count_incell_sfr100_cloudy = count_incell_sfr100_cloudy + 1
                 if (in_cell(i) >= 1) count_in4cell_sfr100_cloudy = count_in4cell_sfr100_cloudy + 1
                 count_sfr100_cloudy = count_sfr100_cloudy + 1

              endif
           endif
        endif


!!!if (density_amr(i) >= 10 .and. temperature_post <= 1d4) then
!!!    SFR_hybrid(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
!!!SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
!!!!print*, SFR_amr(i), SFR_amr_max
!!!if (SFR_hybrid(i) > SFR_amr_max) then
!!!SFR_hybrid(i) = SFR_amr_max
!!!!print*, 'Reduction'
!!!endif
!!!    if (in_cell(i) == 1) count_incell_sfr_hybrid = count_incell_sfr_hybrid + 1
!!!    if (in_cell(i) >= 1) count_in4cell_sfr_hybrid = count_in4cell_sfr_hybrid + 1
!!!    count_sfr_hybrid = count_sfr_hybrid + 1
!!!endif

        SFR_amr_max = 0.3d0*9.9d0*scale_SFR_max*density_amr(i)*cell_size_amr(i)**3
        SFR0_hybrid(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
        !print*, SFR1_amr(i), SFR_amr_max
        if (SFR0_hybrid(i) > SFR_amr_max) then
           SFR0_hybrid(i) = SFR_amr_max
           !print*, 'Reduction'
        endif
        if (in_cell(i) == 1) count_incell_sfr0_hybrid = count_incell_sfr0_hybrid + 1
        if (in_cell(i) >= 1) count_in4cell_sfr0_hybrid = count_in4cell_sfr0_hybrid + 1
        count_sfr0_hybrid = count_sfr0_hybrid + 1

        if (density_amr(i) >= 1) then
           SFR1_hybrid(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
           !print*, SFR1_amr(i), SFR_amr_max
           if (SFR1_hybrid(i) > SFR_amr_max) then
              SFR1_hybrid(i) = SFR_amr_max
              !print*, 'Reduction'
           endif
           if (in_cell(i) == 1) count_incell_sfr1_hybrid = count_incell_sfr1_hybrid + 1
           if (in_cell(i) >= 1) count_in4cell_sfr1_hybrid = count_in4cell_sfr1_hybrid + 1
           count_sfr1_hybrid = count_sfr1_hybrid + 1


           if (density_amr(i) >= 10) then
              SFR10_hybrid(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
              !print*, SFR10_amr(i), SFR_amr_max
              if (SFR10_hybrid(i) > SFR_amr_max) then
                 SFR10_hybrid(i) = SFR_amr_max
                 !print*, 'Reduction'
              endif
              if (in_cell(i) == 1) count_incell_sfr10_hybrid = count_incell_sfr10_hybrid + 1
              if (in_cell(i) >= 1) count_in4cell_sfr10_hybrid = count_in4cell_sfr10_hybrid + 1
              count_sfr10_hybrid = count_sfr10_hybrid + 1

              if (density_amr(i) >= 100) then
                 SFR100_hybrid(i) = epsilon*scale_SFR*density_amr(i)**n_kennicutt*cell_size_amr(i)**3 ! en M_sun/yr
                 !print*, SFR100_amr(i), SFR_amr_max
                 if (SFR100_hybrid(i) > SFR_amr_max) then
                    SFR100_hybrid(i) = SFR_amr_max
                    !print*, 'Reduction'
                 endif
                 if (in_cell(i) == 1) count_incell_sfr100_hybrid = count_incell_sfr100_hybrid + 1
                 if (in_cell(i) >= 1) count_in4cell_sfr100_hybrid = count_in4cell_sfr100_hybrid + 1
                 count_sfr100_hybrid = count_sfr100_hybrid + 1

              endif !check
           endif
        endif


     endif


     !if (count_incell_sfr_amr > 0 .and. in_cell(i) > 0) then
     !print*, 'Nb de cellules avec SFR>0 :   (cellule AMR ', i, ' sur ', n_max, ' ).'
     !print*, 'AMR : ', count_sfr_amr, ', AMR in_cell : ', count_incell_sfr_amr, &
     !                                &', AMR in_4cell : ', count_in4cell_sfr_amr
     !print*, 'CLOUDY : ', count_sfr_cloudy, ', CLOUDY in_cell : ', count_incell_sfr_cloudy, &
     !                                      &', CLOUDY in_4cell : ', count_in4cell_sfr_cloudy
     !print*, 'HYBRID : ', count_sfr_hybrid, ', HYBRID in_cell : ', count_incell_sfr_hybrid, &
     !                                      &', HYBRID in_4cell : ', count_in4cell_sfr_hybrid
     !endif


     write(14,'(30d20.12,1I3,A5,A35)') x_amr(i), y_amr(i), z_amr(i), distance_min, x_lop(j_min), y_lop(j_min), z_lop(j_min), &
          & theta_lop(j_min), phi_lop(j_min), depth_lop(j_min), density_lop(j_min), temperature_post, &
          & Hneutre_lop(j_min), Oneutre_lop(j_min), Hb_4861A_lop(j_min), OIII_5007A_lop(j_min), Ha_6563A_lop(j_min), &
          & NII_6584A_lop(j_min), SFR100_amr(i), SFR100_cloudy(i), SFR100_hybrid(i), &
          & SFR10_amr(i), SFR10_cloudy(i), SFR10_hybrid(i), &
          & SFR1_amr(i), SFR1_cloudy(i), SFR1_hybrid(i), &
          & SFR0_amr(i), SFR0_cloudy(i), SFR0_hybrid(i), &
          & in_cell(i), '  '//flag(j_min), '  '//name(j_min)

     if (density_lop(j_min) == 0 .and. temperature_post == 0 .and. Hneutre_lop(j_min) == 0 .and. Hb_4861A_lop(j_min) == 0 &
          & .and. OIII_5007A_lop(j_min) == 0 .and. Ha_6563A_lop(j_min)  == 0 .and. NII_6584A_lop(j_min) == 0) then
        print*, 'Attention, ecriture de valeurs nulles. ', j_min
        print*,density_lop(j_min), temperature_post, &
             & Hneutre_lop(j_min), Hb_4861A_lop(j_min), OIII_5007A_lop(j_min), Ha_6563A_lop(j_min), &
             & NII_6584A_lop(j_min)
        stop
     endif


     !calcul de la masse totale : M = rho*cell_size**3 /!\ rho en H/cc et cell_size en kpc...
     ! 1 cm = 3.24d-22 kpc
     ! M_H = 1.67d-27 kg
     ! M_sun = 2.0d30 kg
     ! 1 M_sun/pc^3 = 1/40 H/cc  !! 1/30 avec corrections ?
     M_tot_amr = M_tot_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass


     if (in_cell(i) > 0) then
        M_in4cell_amr = M_in4cell_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun
        M_in4cell_cloudy = M_in4cell_cloudy + density_lop(j_min)*cell_size_amr(i)**3*scale_mass ! en M_sun
        if (temperature_post > temperature_amr(i)) then
           M_in4cell_heated_amr = M_in4cell_heated_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass! en M_sun
           M_in4cell_heated_cloudy = M_in4cell_heated_cloudy + density_lop(j_min)*cell_size_amr(i)**3*scale_mass ! en M_sun
        endif

        SFR100_in4Cell_amr  = SFR100_in4cell_amr + SFR100_amr(i)
        SFR100_in4cell_cloudy  = SFR100_in4cell_cloudy + SFR100_cloudy(i)
        SFR100_in4cell_hybrid  = SFR100_in4cell_hybrid + SFR100_hybrid(i)
        SFR10_in4Cell_amr  = SFR10_in4cell_amr + SFR10_amr(i)
        SFR10_in4cell_cloudy  = SFR10_in4cell_cloudy + SFR10_cloudy(i)
        SFR10_in4cell_hybrid  = SFR10_in4cell_hybrid + SFR10_hybrid(i)
        SFR1_in4Cell_amr  = SFR1_in4cell_amr + SFR1_amr(i)
        SFR1_in4cell_cloudy  = SFR1_in4cell_cloudy + SFR1_cloudy(i)
        SFR1_in4cell_hybrid  = SFR1_in4cell_hybrid + SFR1_hybrid(i)
        SFR0_in4Cell_amr  = SFR0_in4cell_amr + SFR0_amr(i)
        SFR0_in4cell_cloudy  = SFR0_in4cell_cloudy + SFR0_cloudy(i)
        SFR0_in4cell_hybrid  = SFR0_in4cell_hybrid + SFR0_hybrid(i)
        if (temperature_post < temperature_amr(i)) then
           print*, 'Probleme ? ', i, j_min, 'AMR ', temperature_amr(i), '> LOP', temperature_post, &
                & (temperature_amr(i)-temperature_post)/temperature_amr(i)*100.d0
        endif

        if (in_cell(i) == 1) then
           M_incell_amr = M_incell_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun
           M_incell_cloudy = M_incell_cloudy + density_lop(j_min)*cell_size_amr(i)**3*scale_mass! en M_sun
           if (temperature_post > temperature_amr(i)) then
              M_incell_heated_amr = M_incell_heated_amr + density_amr(i)*cell_size_amr(i)**3*scale_mass ! en M_sun
              M_incell_heated_cloudy = M_incell_heated_cloudy + density_lop(j_min)*cell_size_amr(i)**3*scale_mass ! en M_sun
           endif

           SFR100_incell_amr  = SFR100_incell_amr + SFR100_amr(i)
           SFR100_incell_cloudy  = SFR100_incell_cloudy + SFR100_cloudy(i)
           SFR100_incell_hybrid  = SFR100_incell_hybrid + SFR100_hybrid(i)
           SFR10_incell_amr  = SFR10_incell_amr + SFR100_amr(i)
           SFR10_incell_cloudy  = SFR10_incell_cloudy + SFR10_cloudy(i)
           SFR10_incell_hybrid  = SFR10_incell_hybrid + SFR10_hybrid(i)
           SFR1_incell_amr  = SFR1_incell_amr + SFR100_amr(i)
           SFR1_incell_cloudy  = SFR1_incell_cloudy + SFR1_cloudy(i)
           SFR1_incell_hybrid  = SFR1_incell_hybrid + SFR1_hybrid(i)
           SFR0_incell_amr  = SFR0_incell_amr + SFR100_amr(i)
           SFR0_incell_cloudy  = SFR0_incell_cloudy + SFR0_cloudy(i)
           SFR0_incell_hybrid  = SFR0_incell_hybrid + SFR0_hybrid(i)
        endif
     endif

     !print*, 'min/max de x_lop : ', minval(x_lop),'/',maxval(x_lop)
     !print*, 'min/max de y_lop : ', minval(y_lop),'/',maxval(y_lop)
     !print*, 'min/max de z_lop : ', minval(z_lop),'/',maxval(z_lop)
     !print*, x_amr(i), y_amr(i), z_amr(i)

  end do

  count_in_cell = sum(in_cell, MASK = in_cell .eq. 1) !nb de cellules AMR dont le pt cloudy le plus proche est DANS la cellule
  count_in_4cell = count_in_cell + int(sum(in_cell, MASK = in_cell .eq. 4)/4.d0) !nb de cellules AMR dont le pt cloudy le plus proche est dans 4*taille_cellule

  print*, 'Nb de cellules avec SFR100>0 (n > 100 H/cc) :  '
  print*, 'AMR : ', count_sfr100_amr, ', AMR in_cell : ', count_incell_sfr100_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr100_amr
  print*, 'CLOUDY : ', count_sfr100_cloudy, ', CLOUDY in_cell : ', count_incell_sfr100_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr100_cloudy
  print*, 'HYBRID : ', count_sfr100_hybrid, ', HYBRID in_cell : ', count_incell_sfr100_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr100_hybrid
  print*, 'Nb de cellules avec SFR10>0 (n > 10 H/cc) :  '
  print*, 'AMR : ', count_sfr10_amr, ', AMR in_cell : ', count_incell_sfr10_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr10_amr
  print*, 'CLOUDY : ', count_sfr10_cloudy, ', CLOUDY in_cell : ', count_incell_sfr10_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr10_cloudy
  print*, 'HYBRID : ', count_sfr10_hybrid, ', HYBRID in_cell : ', count_incell_sfr10_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr10_hybrid
  print*, 'Nb de cellules avec SFR1>0 (n > 1 H/cc) :  '
  print*, 'AMR : ', count_sfr1_amr, ', AMR in_cell : ', count_incell_sfr1_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr1_amr
  print*, 'CLOUDY : ', count_sfr1_cloudy, ', CLOUDY in_cell : ', count_incell_sfr1_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr1_cloudy
  print*, 'HYBRID : ', count_sfr1_hybrid, ', HYBRID in_cell : ', count_incell_sfr1_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr1_hybrid
  print*, 'Nb de cellules avec SFR0>0 (pas de critere sur n) :  '
  print*, 'AMR : ', count_sfr0_amr, ', AMR in_cell : ', count_incell_sfr0_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr0_amr
  print*, 'CLOUDY : ', count_sfr0_cloudy, ', CLOUDY in_cell : ', count_incell_sfr0_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr0_cloudy
  print*, 'HYBRID : ', count_sfr0_hybrid, ', HYBRID in_cell : ', count_incell_sfr0_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr0_hybrid

  if (mac) then
     open(15,file='/Users/oroos/Post-stage/LOPs'//trim(sim)//'/'//trim(nm)//'mass_sfr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (curie) then
     open(15,file='/ccc/work/cont005/gen2192/juneaus/'//trim(nm)//'mass_sfr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (irfucoast) then
     open(15,file='/gpfs/data2/oroos/'//trim(nm)//'mass_sfr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (curie2) then
     open(15,file='/ccc/work/cont003/dsm/rooso/Tests_c13.02d0/'//trim(nm)//'mass_sfr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  else if (trim(host) == 'cloudy_lores') then
     open(15,file='/ccc/scratch/cont003/dsm/rooso/Simu_lores_Cloudy/'//trim(nm)//'lores_mass_sfr_'&
          & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat',form='formatted')
  endif

  write(15,*) '-------------------------------------------------------------------------------------'
  write(15,*) 'Pour les cellules AMR de ', n_min, ' a ', n_max, ' sur ', number_of_leaves, ' : on a '
  write(15,*)  'Masse totale contenue initialement dans les cellules AMR : ', M_tot_amr, ' M_sun.'
  write(15,*)  'SFR100 total initial des cellules AMR : ', sum(SFR100_amr), 'M_sun/yr.'
  write(15,*)  'SFR10  total initial des cellules AMR : ', sum(SFR10_amr), 'M_sun/yr.'
  write(15,*)  'SFR1   total initial des cellules AMR : ', sum(SFR1_amr), 'M_sun/yr.'
  write(15,*)  'SFR0   total initial des cellules AMR : ', sum(SFR0_amr), 'M_sun/yr.'
  write(15,*) ''
  write(15,*) 'Pour les cellules AMR dont le point le plus proche est inclus dans 4x taille de la cellule :'
  write(15,*) '4x - Masse contenue initialement dans les cellules AMR : ', M_in4cell_amr, ' M_sun, soit '&
       &, M_in4cell_amr/M_tot_amr*100, ' % du total.'
  write(15,*) '4x - Masse contenue dans les cellules AMR apres traitement CLOUDY (densite re-echantillonnee par cloudy) : ',&
       & M_in4cell_cloudy, ' M_sun.'
  write(15,*) '4x - Soit une erreur de : ', abs(M_in4cell_cloudy-M_in4cell_amr)/M_in4cell_amr*100.d0, ' %.'
  write(15,*) '4x - Masse chauffee correspondante (densite amr) : ', M_in4cell_heated_amr, ' M_sun, soit : '&
       &, M_in4cell_heated_amr/M_in4cell_amr*100.d0, ' %.'
  write(15,*) '4x - Masse chauffee correspondante (densite cloudy) : ', M_in4cell_heated_cloudy, ' M_sun, soit : '&
       &, M_in4cell_heated_cloudy/M_in4cell_cloudy*100.d0, ' %.'
  write(15,*) ''
  write(15,*) '4x - SFR100 initial des cellules AMR : ', SFR100_in4cell_amr, 'M_sun/yr.'
  write(15,*) '4x - SFR100 des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR100_in4cell_hybrid, 'M_sun/yr.'
  write(15,*) '4x - SFR100 des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR100_in4cell_cloudy, 'M_sun/yr.'
  write(15,*) '4x - SFR10  initial des cellules AMR : ', SFR10_in4cell_amr, 'M_sun/yr.'
  write(15,*) '4x - SFR10  des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR10_in4cell_hybrid, 'M_sun/yr.'
  write(15,*) '4x - SFR10  des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR10_in4cell_cloudy, 'M_sun/yr.'
  write(15,*) '4x - SFR1   initial des cellules AMR : ', SFR1_in4cell_amr, 'M_sun/yr.'
  write(15,*) '4x - SFR1   des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR1_in4cell_hybrid, 'M_sun/yr.'
  write(15,*) '4x - SFR1   des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR1_in4cell_cloudy, 'M_sun/yr.'
  write(15,*) '4x - SFR0   initial des cellules AMR : ', SFR0_in4cell_amr, 'M_sun/yr.'
  write(15,*) '4x - SFR0   des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR0_in4cell_hybrid, 'M_sun/yr.'
  write(15,*) '4x - SFR0   des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR0_in4cell_cloudy, 'M_sun/yr.'

  write(15,*) ''
  write(15,*) 'Pour les cellules AMR dont le point le plus proche est inclus dans la cellule :'
  write(15,*) 'Masse contenue initialement dans les cellules AMR : ', M_incell_amr, ' M_sun, soit'&
       &, M_incell_amr/M_tot_amr*100, ' % du total.'
  write(15,*) 'Masse contenue dans les cellules AMR apres traitement CLOUDY (densite re-echantillonnee par cloudy) : ',&
       & M_incell_cloudy, ' M_sun.'
  write(15,*) 'Soit une erreur de : ', abs(M_incell_cloudy-M_incell_amr)/M_incell_amr*100.d0, ' %.'
  write(15,*) 'Masse chauffee correspondante (densite amr) : ', M_incell_heated_amr, ' M_sun, soit : '&
       &, M_incell_heated_amr/M_incell_amr*100.d0, ' %.'
  write(15,*) 'Masse chauffee correspondante (densite cloudy) : ', M_incell_heated_cloudy, ' M_sun, soit : '&
       &, M_incell_heated_cloudy/M_incell_cloudy*100.d0, ' %.'
  write(15,*) ''
  write(15,*) 'SFR100 initial des cellules AMR : ', SFR100_incell_amr, 'M_sun/yr.'
  write(15,*) 'SFR100 des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR100_incell_hybrid, 'M_sun/yr.'
  write(15,*) 'SFR100 des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR100_incell_cloudy, 'M_sun/yr.'
  write(15,*) 'SFR10  initial des cellules AMR : ', SFR10_incell_amr, 'M_sun/yr.'
  write(15,*) 'SFR10  des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR10_incell_hybrid, 'M_sun/yr.'
  write(15,*) 'SFR10  des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR10_incell_cloudy, 'M_sun/yr.'
  write(15,*) 'SFR1   initial des cellules AMR : ', SFR1_incell_amr, 'M_sun/yr.'
  write(15,*) 'SFR1   des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR1_incell_hybrid, 'M_sun/yr.'
  write(15,*) 'SFR1   des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR1_incell_cloudy, 'M_sun/yr.'
  write(15,*) 'SFR0   initial des cellules AMR : ', SFR0_incell_amr, 'M_sun/yr.'
  write(15,*) 'SFR0   des cellules AMR apres traitement CLOUDY (densite amr) : ', SFR0_incell_hybrid, 'M_sun/yr.'
  write(15,*) 'SFR0   des cellules AMR apres traitement CLOUDY (densite cloudy) : ', SFR0_incell_cloudy, 'M_sun/yr.'
  write(15,*) ''
  write(15,*) 'Min/max de density_amr : ', minval(density_amr),'/',maxval(density_amr), ' H/cc.'
  write(15,*) 'Min/max de density_lop : ', minval(density_lop),'/',maxval(density_lop), ' H/cc.'
  write(15,*) ''
  write(15,*) 'Sur les ', n_max-n_min, ' cellules de l intervalle : '
  write(15,*)  count_in_4cell, ' cellules ont le point CLOUDY le plus proche inclus dans 4x taille de la cellule, soit : ', &
       & count_in_4cell/(n_max-n_min)*100.d0, '% des cellules.'
  write(15,*)  count_in_cell, ' cellules ont le point CLOUDY le plus proche inclus dans la cellule, soit : ', &
       & count_in_cell/(n_max-n_min)*100.d0, '% des cellules.'
  write(15,*) ''
  write(15,*) 'Nombre de cellules chauffees : (toutes les cellules amr) : ', nb_heated_cells
  write(15,*) 'Nombre de cellules chauffees : (cellules amr dont le point cloudy le plus proche est dans 4x taille cellule) : ', &
       & nb_heated_in4cell_cells
  write(15,*) 'Nombre de cellules chauffees : (cellules amr dont le point cloudy le plus proche est dans la cellule)) : ', &
       & nb_heated_incell_cells
  write(15,*) ''
  write(15,*) 'Nb de cellules avec SFR100>0 (n > 100 H/cc) :  '
  write(15,*) 'AMR : ', count_sfr100_amr, ', AMR in_cell : ', count_incell_sfr100_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr100_amr
  write(15,*) 'CLOUDY : ', count_sfr100_cloudy, ', CLOUDY in_cell : ', count_incell_sfr100_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr100_cloudy
  write(15,*) 'HYBRID : ', count_sfr100_hybrid, ', HYBRID in_cell : ', count_incell_sfr100_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr100_hybrid
  write(15,*) 'Nb de cellules avec SFR10>0 (n > 10 H/cc) :  '
  write(15,*) 'AMR : ', count_sfr10_amr, ', AMR in_cell : ', count_incell_sfr10_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr10_amr
  write(15,*) 'CLOUDY : ', count_sfr10_cloudy, ', CLOUDY in_cell : ', count_incell_sfr10_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr10_cloudy
  write(15,*) 'HYBRID : ', count_sfr10_hybrid, ', HYBRID in_cell : ', count_incell_sfr10_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr10_hybrid
  write(15,*) 'Nb de cellules avec SFR1>0 (n > 1 H/cc) :  '
  write(15,*) 'AMR : ', count_sfr1_amr, ', AMR in_cell : ', count_incell_sfr1_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr1_amr
  write(15,*) 'CLOUDY : ', count_sfr1_cloudy, ', CLOUDY in_cell : ', count_incell_sfr1_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr1_cloudy
  write(15,*) 'HYBRID : ', count_sfr1_hybrid, ', HYBRID in_cell : ', count_incell_sfr1_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr1_hybrid
  write(15,*) 'Nb de cellules avec SFR0>0 (pas de critere sur n) :  '
  write(15,*) 'AMR : ', count_sfr0_amr, ', AMR in_cell : ', count_incell_sfr0_amr, &
       &', AMR in_4cell : ', count_in4cell_sfr0_amr
  write(15,*) 'CLOUDY : ', count_sfr0_cloudy, ', CLOUDY in_cell : ', count_incell_sfr0_cloudy, &
       &', CLOUDY in_4cell : ', count_in4cell_sfr0_cloudy
  write(15,*) 'HYBRID : ', count_sfr0_hybrid, ', HYBRID in_cell : ', count_incell_sfr0_hybrid, &
       &', HYBRID in_4cell : ', count_in4cell_sfr0_hybrid
  write(15,*) ''

  !- jusqu'à présent, mes cartes et valeurs de SFR montraient les cas 'pas de FB dans la simu + FB cloudy' versus 'FB dans la simu + FB cloudy'
  !et donc T_f = max (T_cloudy, T_amr), T_i = T_amr (la meme)

  !- maintenant on veut l'impact du FB global simu+cloudy
  !donc on voudrait T_f = max(T_cloudy, T_amr_avec_fb) avec le calcul cloudy fait sur le cube avec fb, T_i = T_amr_sans_fb


  close(10)
  close(11)
  close(12)
  close(14)
  close(15)

  deallocate(name, x_amr, y_amr, z_amr, vx_amr, vy_amr, vz_amr, density_amr, temperature_amr, &
       & cell_size_amr, ilevel_amr, x_lop, y_lop, z_lop, theta_lop, phi_lop, depth_lop, density_lop, &
       & temperature_lop, Hneutre_lop, Oneutre_lop, Hb_4861A_lop, OIII_5007A_lop, Ha_6563A_lop, NII_6584A_lop, &
       & remaining_lop_points, SFR100_amr, SFR100_cloudy,SFR100_hybrid, SFR10_amr, SFR10_cloudy,SFR10_hybrid, &
       & SFR1_amr, SFR1_cloudy,SFR1_hybrid,SFR0_amr, SFR0_cloudy,SFR0_hybrid)

  print*, 'Fichier cree : '//trim(nm)//'cloudy_near_amr_'&
       & //trim(sim)//'_'//trim(part)//'_from'//trim(nmin)//'to'//trim(nmax)//'.dat'

  print*, 'Fin du programme, tout est OK.'

end program post_cloudy_parameters
