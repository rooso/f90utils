program part2map

!! Convert Ramses output on paticles into an ascii file and/or a fits
!! Based on Frederic Bournaud's plot_part
!! Florent Renaud - 1 Aug 2011

  implicit none

  character(len=256)::info_file, file
  character(len=13)::info_text
  integer::ncpu, ipart
  character(len=5)::ncharcpu
  real(kind=8),dimension(:),allocatable::age
  integer,dimension(:),allocatable::id

  integer,parameter::NPARTMAX=60000000
  integer::n, i, j, k, ndim, npart, nselect
!  integer::fpart=0, lpart=0, Ndm, Nsb, Nsd
  integer::Ndm, Nsb, Nsd
  real(kind=8)::xmin=0., ymin=0., zmin=0., xmax=1., ymax=1., zmax=1., cube=0, boxlen=1.0, ctr1, ctr2, time
  real(kind=8)::agemyr, agemyrmin=0.0, agemyrmax=1.0D9, massmin=0.0D0, massmax=0.0D0
  real(kind=8)::xxmin, yymin, zzmin, xxmax, yymax, zzmax
  
  integer, parameter::px=512, py=512
  integer::a, b, axe1, axe2, axe3
  real(KIND=4)::l1, l2, l3, l4, eps1, eps2, eps3, eps4, dx, dy
  character(len=1)::dir

  real(kind=8),dimension(:,:),allocatable::x,v
  real(kind=8),dimension(:),allocatable::m
  real(kind=8),dimension(1:NPARTMAX,1:3)::xselect
  real(kind=8),dimension(1:NPARTMAX)::mselect

  real(kind=4),dimension(:,:),allocatable::rhosurf

  character(len=5)::char_ioutput
  character(len=20)::ic_info_text

  character(80)::filename
  integer::status, unit, blocksize, bitpix, naxis
  integer,dimension(2)::naxes
  integer::group, fpixel, nelements
  logical::simple, extend
  character(2)::num

  integer::step=1
  logical::ok, star=.false., newstar=.false., ascii=.false., idout=.false., kpc=.false., kpcx=.false.
  
  character(len=128)::repository, info='', suffix=''
  character(len=4)::typ

  real(kind=8)::scale_vkms,scale_t,scale_l,scale_d,scale_s,scale_m


  call read_params
   

  ! Read ramses data

  ! get the snapshot id from the repository
  if(index(trim(repository), '/', .true.)==len_trim(repository)) then
    write(char_ioutput,'(a5)') repository(len_trim(repository)-5:len_trim(repository)-1)
  else
    write(char_ioutput,'(a5)') repository(len_trim(repository)-4:len_trim(repository))
  endif

  if(ascii)then
    filename='part_'//TRIM(char_ioutput)//TRIM(suffix)//'.dat'
    open(unit=2, file=filename, form='formatted')

    if(idout)then
      filename='idpart_'//TRIM(char_ioutput)//TRIM(suffix)//'.dat'
      open(unit=3, file=filename, form='formatted')
    endif
  endif

  info_file=trim(trim(repository) // '/info_' // trim(char_ioutput) // '.txt')
  inquire(file=trim(info_file), exist=ok)
  if (.NOT. ok) then
    write(*,*) "Error: ", trim(info_file), " not found"
    stop
  endif
    
  ! Read output timestep
  open(unit=1, file=info_file, form='formatted', status='old')
  read(1,'(a13,I11)') info_text, ncpu
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,'(a13,E23.15)') info_text, boxlen
  read(1,'(a13,E23.15)') info_text, time
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,*)
  read(1,'(a13,E23.15)') info_text, scale_l
  read(1,'(a13,E23.15)') info_text, scale_d
  read(1,'(a13,E23.15)') info_text, scale_t
  close(1)
  
!  output Ramses Merger: kpc, 1e9 Msun
! conversion to CGS:
!  scale_l  = 3.08567752D21  ! = 1 kpc                   in cgs
!  scale_m  = 1.9889D42      ! = 1e9 Msun                in cgs
!  scale_d  = 6.77025D-23    ! = 1e9 Msun / kpc^3        in cgs
!  scale_t  = 4.7043D14      ! = ramses time unit        in cgs
!  scale_v  = 6.559269D6     ! = ramses velocity unit    in cgs
! conversion to useful units: multiply the Ramses output with the scale_X
!  scale_vkms = 65.59269  ! = ramses velocity    in km/s
!  scale_t    = 14.9070   ! = ramses time        in Myr
  scale_vkms = scale_l / scale_t / 1D5
  scale_m = scale_d * scale_l**3 / 1.9891D33
  scale_d = scale_d / 1.9891D33 * (3.085677581282D21)**3
  scale_l = scale_l / 3.085677581282D21
  scale_t = scale_t / 3.15576D13
  scale_s = scale_m / scale_l**2
! velocity * scale_vkms = km / s
! mass * scale_m = Msun
! density * scale_d = Msun / kpc^3
! length * scale_l = kpc
! time * scale_t = Myr
! surface_density * scale_s = Msun / kpc^2

  time = time*scale_t


  if(kpcx) then
    if(cube > 0.0) then
      cube = cube/2.
      xmax = xmin + cube
      ymax = ymin + cube
      zmax = zmin + cube
      xmin = xmin - cube
      ymin = ymin - cube
      zmin = zmin - cube
    endif

    xmin = xmin/boxlen + 0.5
    xmax = xmax/boxlen + 0.5
    ymin = ymin/boxlen + 0.5
    ymax = ymax/boxlen + 0.5
    if(kpc) then
      zmin = zmin/boxlen + 0.5
      zmax = zmax/boxlen + 0.5
    endif
  endif

  xxmin = xmin*boxlen
  yymin = ymin*boxlen
  zzmin = zmin*boxlen
  xxmax = xmax*boxlen
  yymax = ymax*boxlen
  zzmax = zmax*boxlen

  file = trim(trim(repository) // '/part_' // trim(char_ioutput) // '.out')
  inquire(file=trim(file)//'00001', exist=ok)
  if (.NOT. ok) then
    write(*,*) "Error: ", trim(file)//'00001', " not found"
    stop
  endif

  nselect = 0     
  ! Loop over CPU files
  do k=1,ncpu
    write(ncharcpu,fmt='(I5.5)') k
    open(unit=1, file=trim(file)//TRIM(ncharcpu), status='old', form='unformatted')
    read(1)
    read(1)ndim
    read(1)npart
    read(1)
    read(1)
    read(1)
    read(1)
    read(1)
      
    allocate(m(1:npart), x(1:npart,1:ndim), v(1:npart,1:ndim), id(1:npart))
    if(star) allocate(age(1:npart))
                      
    ! read position
    do i=1,ndim
      read(1) m
      x(1:npart,i) = m
    end do 
    ! read velocity
    do i=1,ndim
      read(1) m
      v(1:npart,i) = m
    end do
    read(1) m
    read(1) id
    if(star) then
      read(1)
      read(1) age
    end if

    ! Loop over particles
    do ipart=1, npart, step
        
      if(newstar .eqv. .true.) agemyr = time-age(ipart)*scale_t
      ! particle selection
      if(massmax > 0.0.AND.massmin > 0.0)then
        if(m(ipart)*1e9 > massmax.OR.m(ipart)*1e9 < massmin) goto 666 ! go to next particle
      endif

!      if( (id(ipart)>=0.AND.x(ipart,1)>=xxmin.AND.x(ipart,1)<=xxmax.AND.x(ipart,2)>=yymin.AND.x(ipart,2)<=yymax.AND.x(ipart,3)>=zzmin.AND.x(ipart,3)<=zzmax) .AND. ( ((newstar.eqv. .true.).AND.(age(ipart)>0) .AND. (agemyr >= agemyrmin) .AND. (agemyr < agemyrmax) ) .OR. ((newstar.eqv. .false.).AND.(id(ipart)>=fpart).AND.(id(ipart)<=lpart)) ) )then
      if( (id(ipart)>=0.AND.x(ipart,1)>=xxmin.AND.x(ipart,1)<=xxmax.AND.x(ipart,2)>=yymin.AND.x(ipart,2)<=yymax.AND.x(ipart,3)>=zzmin.AND.x(ipart,3)<=zzmax) .AND. ( ((newstar.eqv. .true.).AND.(age(ipart)>0) .AND. (agemyr >= agemyrmin) .AND. (agemyr < agemyrmax) ) .OR. (newstar.eqv. .false.) ) )then
        nselect=nselect+1
        xselect(nselect,1)=x(ipart,1)
        xselect(nselect,2)=x(ipart,2)
        xselect(nselect,3)=x(ipart,3)
        mselect(nselect+1)=m(ipart)
        if(ascii)then
          if(star)then
            write(2,fmt='(7(1x,E15.8),1x,E15.8)'), x(ipart,:)-0.5*boxlen, v(ipart,:)*scale_vkms, m(ipart)*1e9, agemyr
          else
            write(2,fmt='(7(1x,E15.8))'), x(ipart,:)-0.5*boxlen, v(ipart,:)*scale_vkms, m(ipart)*1e9
          endif

          if(idout)then
            write(3,fmt='(I8.8)'), id(ipart)
          endif
        endif
      end if
      666 continue
    end do
    ! End loop over particles

    deallocate(m,x,v,id)
    if(star) deallocate(age)
      
    close(1)  
  end do
  ! End loop over CPU files


  npart=nselect
  write(*,*) npart,' particles in the selection'

  if(ascii)then
    close(2)
    if(idout)then
      close(3)
    endif
    stop
  endif

    
  allocate(rhosurf(px,py))
  rhosurf = 0.

  select case (dir)
    case ('x')
      axe1=3
      ctr1=(zzmax-zzmin)/2.
      axe2=2
      ctr2=(yymax-yymin)/2.
      axe3=1
    case ('y')
      axe1=1
      ctr1=(xxmax-xxmin)/2.
      axe2=3
      ctr2=(zzmax-zzmin)/2.    
      axe3=2
    case default
      axe1=1
      ctr1=(xxmax-xxmin)/2.
      axe2=2
      ctr2=(yymax-yymin)/2.
      axe3=3
  end select

  ! pixel size
  dx = ctr1*2./px
  dy = ctr2*2./py

  do i=1,npart   
    xselect(i,axe1)=xselect(i,axe1)-boxlen/2.+ctr1 ! centering in the sub-grid xmin-xmax
    a = INT(xselect(i,axe1)/dx)
    xselect(i,axe2)=xselect(i,axe2)-boxlen/2.+ctr2
    b = INT(xselect(i,axe2)/dy)

    if(xselect(i,axe1)<px*dx-dx .AND. xselect(i,axe1)>dx .AND. xselect(i,axe2)<py*dy-dy .AND. xselect(i,axe2)>dy) then
      ! weight in each pixel
      l1 = xselect(i,axe2) - b*dy
      l2 = dy - l1
      l3 = xselect(i,axe1) - a*dx
      l4 = dx - l3
      eps1 = l1 / dy
      eps2 = l2 / dy
      eps3 = l3 / dx
      eps4 = l4 / dx
      
      ! surface density
      rhosurf(a,b) = rhosurf(a,b) + mselect(i)*step*eps2*eps4
      rhosurf(a+1,b) = rhosurf(a+1,b) + mselect(i)*step*eps2*eps3
      rhosurf(a,b+1) = rhosurf(a,b+1) + mselect(i)*step*eps1*eps4
      rhosurf(a+1,b+1) = rhosurf(a+1,b+1) + mselect(i)*step*eps1*eps3
    endif
  enddo

  do i=1,px
    do j=1,py
      rhosurf(i,j) = rhosurf(i,j) / (dx*dy) * scale_s ! Msun / kpc^2
    enddo
  enddo

  ! write surface density in a fits
  status=0
  filename='part_'//TRIM(char_ioutput)//TRIM(suffix)//'.fits'

  call deletefile(filename,status)
  call ftgiou(unit,status)
  blocksize=1
  call ftinit(unit,filename,blocksize,status)
  simple=.true.
  bitpix=-32
  naxis=2
  naxes(1)=px
  naxes(2)=py
  extend=.true.
  call ftphpr(unit,simple,bitpix,naxis,naxes,0,1,extend,status)

  call ftpkyd(unit,'time',time,6,'time',status)
  call ftpkyd(unit,'boxlen',boxlen,6,'boxlen',status)
  call ftpkyj(unit,'idim',axe1,'idim',status)
  call ftpkyj(unit,'jdim',axe2,'jdim',status)
  call ftpkyj(unit,'kdim',axe3,'kdim',status)
  call ftpkyd(unit,'xmin',xmin,6,'xmin',status)
  call ftpkyd(unit,'xmax',xmax,6,'xmax',status)
  call ftpkyd(unit,'ymin',ymin,6,'ymin',status)
  call ftpkyd(unit,'ymax',ymax,6,'ymax',status)
  call ftpkyd(unit,'zmin',zmin,6,'zmin',status)
  call ftpkyd(unit,'zmax',zmax,6,'zmax',status)
  call ftpkys(unit,'outval','Sigma','value',status)
  call ftpkys(unit,'outvalunit','Msun/kpc^2','value unit',status)

  group=1
  fpixel=1
  nelements=naxes(1)*naxes(2)
  call ftppre(unit,group,fpixel,nelements,rhosurf,status)
  call ftclos(unit, status)
  call ftfiou(unit, status)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  contains
  subroutine read_params
    implicit none

    integer::iargc
    character(len=8)::opt
    character(len=128)::arg
    namelist /size/ dir, xmin, xmax, ymin, ymax, zmin, zmax
    namelist /part/ step!, fpart, lpart

    n = iargc()
    if (n < 1) then
      print *, 'usage: part2map -inp input_dir [-nml namelist] [-out suffix] [-star] [-newstar] [-ascii] [-id]'
      print *, ''
      print *, '   If star is set, read age data too.'
      print *, '   If newstar is set, read age data too and only treat particles with positive ages.'
      print *, '   If ascii is set, ouput the data in an ascii file.'
      print *, '   If id is set, output ids of particles extracted in a separate file (ascii only).'
      print *, '   All namelist parameters can be forced if given in the cmd line.'
      print *, ''
      print *, '      -dir   projection (default: z)'
      print *, '      -xmin  selection, also for y and z (default: 0.0)'
      print *, '      -xmax  selection, also for y and z (default: 1.0)'
      print *, '      -step  skip paticles (default: 1 = all particles)'
!      print *, '      -fpart index of the first particle selected'
!      print *, '      -lpart index of the last particle selected'
      print *, '      -mmin  minimum mass of the particles (in Msun)'
      print *, '      -mmin  maximum mass of the particles (in Msun)'      
      print *, '      -amin  minimum age of the new particles (in Myr)'
      print *, '      -amax  maximum age of the new particles (in Myr)'
!      print *, '      -info  info.txt file containing the nb of star/dm particles'
!      print *, '      -typ   star or dm (only used with info, overide f/lpart)'
      stop
    endif

    i = 1
    do while(i.le.n)
       call getarg(i,opt)
       select case (opt)
       case ('-nml')
         call getarg(i+1,arg)
         open(1,file=trim(arg))
         read(1,size)
         read(1,part)
         close(1)
       case ('-inp')
          call getarg(i+1,arg)
          repository = trim(arg)        
       case ('-out')
          call getarg(i+1,arg)
          suffix = trim(arg)
          if(len(TRIM(suffix)).ne.0)suffix = '_'//TRIM(suffix)
       case ('-star')
          star = .true.
          i = i-1
       case ('-newstar')
          star = .true.
          newstar = .true.
          i = i-1
       case ('-ascii')
          ascii = .true.
          i = i-1
       case ('-id')
          idout = .true.
          ascii = .true.
          i = i-1
        case ('-kpc')
          kpc = .true.
          kpcx = .true.
          i = i-1
        case ('-kpcx')
          kpcx = .true.
          i = i-1


       case ('-dir')
          call getarg(i+1,arg)
          dir = trim(arg) 
       case ('-xmin')
          call getarg(i+1,arg)
          read (arg,*) xmin
       case ('-xmax')
          call getarg(i+1,arg)
          read (arg,*) xmax
       case ('-ymin')
          call getarg(i+1,arg)
          read (arg,*) ymin
       case ('-ymax')
          call getarg(i+1,arg)
          read (arg,*) ymax
       case ('-zmin')
          call getarg(i+1,arg)
          read (arg,*) zmin
       case ('-zmax')
          call getarg(i+1,arg)
          read (arg,*) zmax
        case ('-cube')
          call getarg(i+1,arg)
          read (arg,*) cube
          kpc = .true.
          kpcx = .true.
       case ('-step')
          call getarg(i+1,arg)
          read (arg,*) step
!       case ('-fpart')
!          call getarg(i+1,arg)
!          read (arg,*) fpart
!       case ('-lpart')
!          call getarg(i+1,arg)
!          read (arg,*) lpart
       case ('-mmin')
          call getarg(i+1,arg)
          read (arg,*) massmin
       case ('-mmax')
          call getarg(i+1,arg)
          read (arg,*) massmax
       case ('-amin')
          call getarg(i+1,arg)
          read (arg,*) agemyrmin
       case ('-amax')
          call getarg(i+1,arg)
          read (arg,*) agemyrmax
          
!       case ('-info')
!          call getarg(i+1,arg)
!          info = trim(arg)
!       case ('-typ')
!          call getarg(i+1,arg)
!          read (arg,*) typ
       case default
          print '("unknown option ",a8," ignored")', opt
          i = i-1
       end select
       i = i+2
    end do
    return
    
  end subroutine read_params

end program part2map

!=======================================================================
!=======================================================================
!=======================================================================

subroutine deletefile(filename,status) !  Delete a FITS file

  integer::status,unit,blocksize
  character(*)::filename
  
  if (status .gt. 0) return

  call ftgiou(unit,status) ! Get an unused Logical Unit Number
  call ftopen(unit,filename,1,blocksize,status) ! Try to open the file
  
  if (status .eq. 0)then ! file is opened: delete it 
    call ftdelt(unit,status)
  else if (status .eq. 103)then ! file doesn't exist: reset status and clear errors
    status=0
    call ftcmsg
  else ! there was some other error opening the file: delete the file anyway
    status=0
    call ftcmsg
    call ftdelt(unit,status)
  end if
  
  call ftfiou(unit, status) ! Free the unit number

end
